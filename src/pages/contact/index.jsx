import React from "react";
import { connect } from "react-redux";

import { sendContacts } from "../../redux/actions/users.actions/profile.thunk";

import history from "../../history";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { toastr } from "react-redux-toastr";

import "./style.scss";

const mapStateToProps = state => ({
  isSend: state.profile.isSendContacts,
  isAuthenticated: state.auth.isAuthenticated
});

class ContactPage extends React.PureComponent {
  state = {
    name: "",
    email: "",
    message: ""
  };

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  componentDidUpdate(prevProps) {
    if (this.props.isSend !== prevProps.isSend) {
      toastr.success("Sended", "We are sended!");
      history.push("/home");
    }
  }

  render() {
    const { isAuthenticated } = this.props;
    const { name, email, message } = this.state;

    return (
      <>
        <div className="ContactPage">
          <div className="top_header">
          <Header isAuthenticated={isAuthenticated} find={true}></Header>
          </div>
          <main className="ContactPage-content">
            <h1 className="ContactPage-headline">Contact</h1>
            <p className="ContactPage-tagline">
              Got a question? We’d love to hear from you. Send us a message and
              we’ll resoind as soon as possible
            </p>
            <form className="ContactPage-form">
              <div className="ContactPage-form-wrapper">
                <input
                  className="ContactPage-form-input"
                  name="name"
                  type="text"
                  placeholder="Name"
                  value={name}
                  onChange={this.handleChange}
                />
                <input
                  className="ContactPage-form-input"
                  name="email"
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={this.handleChange}
                />
              </div>
              <textarea
                className="ContactPage-form-textarea"
                placeholder="Message"
                name="message"
                value={message}
                onChange={this.handleChange}
              ></textarea>
              <button
                disabled={!email || !name || !message}
                className="ContactPage-form-btn"
                onClick={event => {
                  event.preventDefault();
                  this.props.sendContacts(this.state);
                }}
              >
                Send
              </button>
            </form>
          </main>
        </div>
        <Footer />
      </>
    );
  }
}

export default connect(mapStateToProps, { sendContacts })(ContactPage);
