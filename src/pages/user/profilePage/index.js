import React from 'react';
import { connect } from 'react-redux';
import TopNav from '../../../components/container/TopNav';
import { checkAuth } from '../../../redux/actions/users.actions/auth.thunk';
import { getUser, editUser, deleteUser } from '../../../redux/actions/users.actions/profile.thunk';
import { reduxForm } from 'redux-form';
import Button from '../../../components/styledComp/Button';
import Title from '../../../components/styledComp/Title';
import { Link } from 'react-router-dom';

// import history from '../../history';

export class ProfilePage extends React.PureComponent {
  componentDidMount() {
    // console.log('this.propsdfgdfdf', this.props)
    // this.props.checkAuth();
    this.props.getUser();
  }
  state = {
    email: '',
    first_name: '',
    last_name: '',
    password: ''
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  editHandler = (email, first_name, last_name, password) => {
    this.props.editUser((email, first_name, last_name, password));
  };

  changeEmail = (e) => {
    this.setState({ email: e.target.value });
  };
  changeFirstname = (e) => {
    this.setState({ first_name: e.target.value });
  };
  changeLastname = (e) => {
    this.setState({ last_name: e.target.value });
  };
  changePassword = (e) => {
    this.setState({ password: e.target.value });
  };

  onEdit = async () => {
    await this.props.editUser(
      this.state.email,
      this.state.first_name,
      this.state.last_name,
      this.state.password
    );
  };
  onDelete = async () => {
    await this.props.deleteUser();
  };

  render() {
    if (!this.props.userData || !this.props.userData.email) return null;
    const {
      email = '',
      first_name = '',
      last_name = '',
      password = '',
      role: { role_type = '' }
    } = this.props.userData;
    // const { email, first_name, last_name, password } = userData;

    return (
      <>
        {/* <TopNav /> */}
        <div className="profile">
        <form onSubmit={this.props.handleSubmit(this.editHandler)} className="ui form error">
          <div>
            <Title>User Profile</Title>
          </div>
          <div className="text-center">
            <h3>You are logged in as {role_type}</h3>
          </div>
          <div>
            <label htmlFor="email">Email: {email}</label>
            <input id="email" type="email" onChange={this.changeEmail} />
          </div>

          <div>
            <label htmlFor="firstName">First Name: {first_name}</label>
            <input id="firstName" type="text" onChange={this.changeFirstname} />
          </div>

          <div>
            <label htmlFor="lastName">Last Name: {last_name}</label>
            <input id="lastName" type="text" onChange={this.changeLastname} />
          </div>

          {/* <div>
            <label htmlFor="password">Password</label>
            <input
              id="password"
              // type="password"
              // value={password}
              onChange={this.changePassword}
            />
          </div> */}
          <div className="btnGroup">
            <Button onClick={this.onEdit}>Edit</Button>
            <Button onClick={this.onDelete}>Delete</Button>
            <Link className="btnChangePsd" path to="/update-password">
              Change Password
            </Link>
          </div>
        </form>
        </div>
      </>
    );
  }
}

const formWrapped = reduxForm({
  form: 'ProfilePage'
})(ProfilePage);

export default connect(
  (state) => ({
    userData: state.profile.user,
    loading: state.auth.loading,
    error: state.auth.error
  }),
  {
    checkAuth,
    getUser,
    editUser,
    deleteUser
  }
)(formWrapped);
