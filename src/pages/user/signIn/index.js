import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { createLogin } from "../../../redux/actions/users.actions/auth.thunk";
import styled from "styled-components";
import { FullLogoBlack } from "../../../components/Header/FullLogo";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col
} from "reactstrap";

import Header from "../../../components/Header";
import Footer from "../../../components/Footer";
import "./index.scss";

const mapStateToProps = state => ({
  loading: state.auth.loginLoading
});

const LogoStyled = styled.img`
  width: 35px;
  height: 35px;
`;

const HeaderStyled = styled.div`
  display: flex;
  justify-content: center;
  width: 95%;
  margin: 35px auto 0;
  padding-bottom: 10px;

  > h1 {
    font-weight: bold;
    margin-left: 3%;
  }
`;

const FormStyled = styled.form`
  width: 620px;
  min-height: 524px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

// const VenueButtonStyled = styled.button`
//   width: 223px;
//   position: relative;
//   height: 47px;
//   background-color: #078089;
//   font-family: "Inter";
//   border-radius: 25px;
//   border: none;
//   color: #ffffff;
//   margin-top: 38px;
//   font-size: 16px;
//   font-weight: 700;
//   left: 50%;
//   transform: translate(-50%, 0);

// `;

const VenueButtonStyled = styled.button`
  height: 50px;
  border-radius: 5px;
  background-color: #0fc3c5;
  width: 90%;
  border: 1px solid #0fc3c5;
  font-size: 20px;
  line-height: 36px;
  color: #ffffff;
  font-family: "Myriad Pro";
  text-align: center;
  
`;

const ModalFooterStyled = styled(ModalFooter)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: none;
  padding: 35px 0px 0px;

  > a {
    color: #04d2ff !important;
  }
`;

const renderInput = ({ input, meta, type, placeholder }) => {
  const className = `signIn-field ${meta.error && meta.touched ? "error" : ""}`;
  return (
    <div className={className}>
      <input
        className="sing-in-input"
        placeholder={placeholder + "*"}
        className="py-3"
        {...input}
        autoComplete="on"
        type={type}
      />
      {renderError(meta)}
    </div>
  );
};

const renderError = ({ error, touched }) => {
  if (touched && error) {
    return (
      <div className="ui error message">
        <div className="header">{error}</div>
      </div>
    );
  }
};

class SignIn extends React.PureComponent {
  loginHandler = async formValues => {
    await this.props.createLogin(formValues);
  };

  render() {
    return (
      <div className="signIn">
        <div className="wrapper-container top_header">
          <div className="container">
            <Header />
          </div>
        </div>
        <main>
          <div className="form-wrapper">
            <FormStyled
              onSubmit={this.props.handleSubmit(this.loginHandler)}
              className="ui form error"
            >
              <HeaderStyled>
                <div className="login-logo-wrapper">
                  <FullLogoBlack />
                </div>
              </HeaderStyled>
              <Field
                placeholder="Email"
                name="email"
                type="email"
                component={renderInput}
                label="E-mail"
              />
              <Field
                placeholder="Password"
                name="password"
                type="password"
                component={renderInput}
                label="Password"
              />
              <Row >
                <Col sm="12" classname="align-to-center">
                  <VenueButtonStyled>Submit</VenueButtonStyled>
                </Col>
              </Row>
              <div className="signIn__form-footer">
                <p className="login-signIn">
                  <span className="another-pages-text">
                    Don’t have an account?
                  </span>{" "}
                  <Link to="/signup" className="login-another-pages">
                    Sign Up
                  </Link>{" "}
                </p>
                <Link className="login-another-pages" to="/check-email">
                  Forgot Password?
                </Link>
              </div>
            </FormStyled>
          </div>
        </main>
        <div className="wrapper-container">
          <Footer />
        </div>
      </div>
    );
  }
}

const validate = formValues => {
  const errors = {};
  if (!formValues.email) {
    errors.email = "You must enter an email";
  }
  if (!formValues.password) {
    errors.password = "You must enter a password";
  }
  return errors;
};

const formWrapped = reduxForm({
  form: "Login",
  validate
})(SignIn);

export default connect(mapStateToProps, { createLogin })(formWrapped);
