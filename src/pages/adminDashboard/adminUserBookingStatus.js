import React, { Component, Fragment } from 'react';
import { Booking, Transaction, User, Venue } from '../../adminDashboardImages';

export class AdminUserBookingStatus extends Component {
  render() {
    return (
      <>
        <div className="card_row ">
          <div className="card_col">
            <span>
              <img src={Booking} alt="icon" />{' '}
            </span>
            <h2 className="price"> {this.props.bookingStatus.reservationCount}</h2>
            <p>Total Booking</p>
          </div>
          <div className="card_col">
            <span>
              <img src={Transaction} alt="icon" />
            </span>
            <h2 className="price">{this.props.bookingStatus.totalEarnings}</h2>
            <p>Total Earnings</p>
          </div>
          <div className="card_col">
            <span>
              <img src={Venue} alt="icon" />
            </span>
            <h2 className="price">{this.props.bookingStatus.totalvenues}</h2>
            <p>No. of Venue</p>
          </div>
          <div className="card_col">
            <span>
              <img src={User} alt="icon" />
            </span>
            <h2 className="price">{this.props.bookingStatus.totalusers}</h2>
            <p>No. of User</p>
          </div>
        </div>
      </>
    );
  }
}

export default AdminUserBookingStatus;
