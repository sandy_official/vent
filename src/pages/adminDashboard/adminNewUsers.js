import React, { Component, Fragment } from 'react';

export class AdminNewUsers extends Component {
  render() {
    return (
      <>
        <div className="table_left">
          <h3>Host Available</h3>
          <div className="table_wrapper">
            <div className="table-responsive">
              <table id="example" className="table table-striped table-bordered invoice_table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.guest.guest.length
                    ? this.props.guest.guest.map((listValue, index) => {
                        return (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{listValue.first_name}</td>
                            <td>{listValue.last_name}</td>
                            <td>{listValue.email}</td>
                          </tr>
                        );
                      })
                    : ''}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default AdminNewUsers;
