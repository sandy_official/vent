import React, { Component } from 'react';
import AdminUserBookingStatus from './adminUserBookingStatus';
import AdminHostAvailable from './adminHostAvailable';
import AdminNewUsers from './adminNewUsers';
import AdminRecentVenueAdded from './adminRecentVenueAdded';
import AdminTransactions from './adminTransactions';

export class AdminDashboard extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="dash_cntnt">
        <AdminUserBookingStatus bookingStatus={data} />
        <div className="cstm_table paddbtm0">
          <AdminHostAvailable hosts={data} />
          <AdminRecentVenueAdded recentVenue={data} />
        </div>
        <div className="cstm_table">
          <AdminTransactions transactions={data} />
          <AdminNewUsers guest={data} />
        </div>
      </div>
    );
  }
}

export default AdminDashboard;
