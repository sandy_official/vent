import React, { Component, Fragment } from 'react';

export class AdminRecentVenueAdded extends Component {
  render() {
    return (
      <>
        <div className="table_right">
          <h3>Recent Venue Added</h3>
          <div className="table_wrapper">
            <div className="table-responsive">
              <table id="example" className="table table-striped table-bordered invoice_table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email Id</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.recentVenue.venues.rows.length
                    ? this.props.recentVenue.venues.rows.map((listValue, index) => {
                        return (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{listValue.name}</td>
                            <td>{listValue.renter_email}</td>
                            <td>{listValue.description}</td>
                          </tr>
                        );
                      })
                    : ''}

                  {/*                        
                          <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr>
                          <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr>
                          <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr>
                           <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr>
                           <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr>
                           <tr>
                             <td>Aakash Sirdhana</td>
                             <td>999-999-9999</td>
                             <td>akashsirdhana@ongraph.com</td>
                             <td>
                                  <span className="tbl_icn">                           
                                   <a href="#"><i className="fa fa-pencil" aria-hidden="true"></i></a>
                                   <a href="#"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                                </span>
                             </td>
                          </tr> */}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default AdminRecentVenueAdded;
