import React, { Component, Fragment } from 'react';
// import { Rating, Dashboard, Transaction, User, Venue, Booking } from '../../adminDashboardImages';
import { Link } from 'react-router-dom';

export class AdminSidebar extends Component {
  render() {
    const { location } = this.props;
    let urlArr = location.pathname.split('/')[2];

    return (
      <>
        <div className="left_sidebar">
          <ul className="sidebar_menu">
            {localStorage.permissions &&
              JSON.parse(localStorage.getItem('permissions')).map((item, index) => (
                <li key={index}>
                  <Link to={`/admin/${item}`} className={urlArr === { item } ? 'active' : ''}>
                    {/* <img alt="icon" src={Transaction} /> */}
                    <span className="text-capitalize">{item.replace('-', ' ')}</span>
                  </Link>
                </li>
              ))}
          </ul>
        </div>
      </>
    );
  }
}

export default AdminSidebar;
