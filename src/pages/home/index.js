import React from "react";
import { Link } from "react-router-dom";

import styled from "styled-components";
import bg1 from "../../assets/header-bg/bg-1.png";
import bg2 from "../../assets/header-bg/bg-2.png";
import bg3 from "../../assets/header-bg/bg-3.png";
import bg4 from "../../assets/header-bg/bg-4.png";
import bg5 from "../../assets/header-bg/bg-5.png";
import bg6 from "../../assets/header-bg/bg-6.png";

import { connect } from "react-redux";
import { checkAuth } from "../../redux/actions/users.actions/auth.thunk";
import { getRatedPlaces } from "../../redux/actions/places.actions/places.thunk";
import { getLastPlaces } from "../../redux/actions/places.actions/places.thunk";

import { isAuthenticated } from "../../redux/selectors/auth/auth.select";
import { getFirstRatedPlaces } from "../../redux/selectors/places/places.select";
import { getSecondRatedPlaces } from "../../redux/selectors/places/places.select";
import { getLastPlacesSelect } from "../../redux/selectors/places/places.select";

import Header from "../../components/Header";
import Section1 from "./components/Section1";
import RatedPlaces from "./components/RatedPlaces/index";
import LastPlaces from "./components/LastPlaces/index";
import Footer from "../../components/Footer";
import ExpressLove from "./components/ExpressLove";
import DiscoverPlaces from "./components/DiscoverPlaces";
import PopularPlaces from "./components/PopularPlaces";
import Brands from "./components/Brands";
import Testimonials from "./components/Testimonials";

import NewYork from "../../assets/slider/new-york.png";
import USA from "../../assets/slider/usa.png";
import Belgium from "../../assets/slider/belgium.png";
import India from "../../assets/slider/india.png";
import Peru from "../../assets/slider/peru.png";

import "./style.scss";

const places = [
    {
        id: 1,
        name: "New York",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: NewYork
    },
    {
        id: 2,
        name: "USA",
        text: "Lorem ipsum dolor sit amet",
        price: "500.00",
        per: "hour",
        persons: "1000-1500",
        rate: "4.2",
        image: USA
    },
    {
        id: 3,
        name: "Belgium",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Belgium
    },
    {
        id: 4,
        name: "India",
        text: "Lorem ipsum dolor sit amet",
        price: "100.00",
        per: "person",
        persons: "20-25",
        rate: "4.5",
        image: India
    },
    {
        id: 5,
        name: "Peru",
        text: "Lorem ipsum dolor sit amet",
        price: "600",
        per: "hour",
        persons: "1500-2000",
        rate: "4.5",
        image: Peru
    },
    {
        id: 6,
        name: "New York",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: NewYork
    },
    {
        id: 7,
        name: "USA",
        text: "Lorem ipsum dolor sit amet",
        price: "500.00",
        per: "hour",
        persons: "1000-1500",
        rate: "4.2",
        image: USA
    },
    {
        id: 8,
        name: "Belgium",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Belgium
    }
];

const bgs = [bg1, bg2, bg3, bg4, bg5, bg6];

class HomePage extends React.PureComponent {
    componentDidMount() {
        this.props.checkAuth();
        this.props.getRatedPlaces();
        this.props.getLastPlaces();
        setTimeout(this.change, 5000);
    }

    state = {
        background: bgs[0]
    };

    changeBg = bg => {
        this.setState(state => ({ background: bg }));
    };

    change = () => {
        let next = bgs.indexOf(this.state.background) + 1;
        if (next === bgs.length) {
            next = 0;
        }
        this.changeBg(bgs[next]);
        setTimeout(this.change, 5000);
    };

    render() {
        const {
            isAuthenticated,
            firstPlaces,
            secondPlaces,
            lastPlaces
        } = this.props;

        const { background } = this.state;

        // const HeaderBg = styled.div`
        //   background-image: url('${this.state.background}'), url('${bg2}');
        //   opacity: 1, 0;
        //   transition: opacity 1s ease-in-in;;
        // `;

        return (
            <div className="wrapper">
            <div class="top_header">
                <div className="container 0000">
                    <Header isAuthenticated={isAuthenticated} find={true}></Header>
                </div>
                </div>
                {/* <HeaderBg> */}
                <img
                    src={bg1}
                    class="bg-img"
                    style={{ opacity: background === bg1 ? 1 : 0 }}
                />
                <img
                    src={bg2}
                    class="bg-img"
                    style={{ opacity: background === bg2 ? 1 : 0 }}
                />
                <img
                    src={bg3}
                    class="bg-img"
                    style={{ opacity: background === bg3 ? 1 : 0 }}
                />
                <img
                    src={bg4}
                    class="bg-img"
                    style={{ opacity: background === bg4 ? 1 : 0 }}
                />
                <img
                    src={bg5}
                    class="bg-img"
                    style={{ opacity: background === bg5 ? 1 : 0 }}
                />
                <img
                    src={bg6}
                    class="bg-img"
                    style={{ opacity: background === bg6 ? 1 : 0 }}
                />
                <Section1></Section1>
                {/* </HeaderBg> */}
                <main>
                    {/* <div className="container"> */}
                    {/* <RatedPlaces places={places} /> */}
                    <PopularPlaces />
                    <Testimonials />
                    {/* <ExpressLove /> */}
                    <DiscoverPlaces />
                    
                    <Brands />
                    
                </main>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: isAuthenticated(state),
    checkAuthError: state.auth.checkAuthError,
    loginError: state.auth.loginError,
    firstPlaces: getFirstRatedPlaces(state),
    secondPlaces: getSecondRatedPlaces(state),
    lastPlaces: getLastPlacesSelect(state)
});

export default connect(mapStateToProps, {
    checkAuth,
    getRatedPlaces,
    getLastPlaces
})(HomePage);
