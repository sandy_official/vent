import React from "react";

import "./index.scss";
import ExploreIcon from "../../../../assets/icons/explore-icon.png";

import Art from "../../../../assets/discover/art.png";
import Graduation from "../../../../assets/discover/graduation.png";
import Concert from "../../../../assets/discover/concert.png";
import Kickstarter from "../../../../assets/discover/kickstarter.png";
import Wedding from "../../../../assets/discover/wedding.png";
import Charity from "../../../../assets/discover/charity.png";
import Conference from "../../../../assets/discover/conference.png";
import Press from "../../../../assets/discover/press.png";
import Roof from "../../../../assets/discover/roof.png";
import Product from "../../../../assets/discover/product.png";

const discoverPlaces = [
  {
    image: Product,
    name: "Product Release",
    venues: "200"
  },
  {
    image: Roof,
    name: "Roof Top",
    venues: "150"
  },
  {
    image: Press,
    name: "Press Conference",
    venues: "100"
  },
  {
    image: Conference,
    name: "Conference",
    venues: "50"
  },
  {
    image: Charity,
    name: "Charity Event",
    venues: "50"
  },
  {
    image: Wedding,
    name: "Wedding Reception",
    venues: "200"
  },
  {
    image: Kickstarter,
    name: "Kickstarter Video",
    venues: "150"
  },
  {
    image: Concert,
    name: "Concert",
    venues: "100"
  },
  {
    image: Graduation,
    name: "Graduation Ceremony",
    venues: "50"
  },
  {
    image: Art,
    name: "Art Exhibit",
    venues: "50"
  }
];

const DiscoverPlaces = () => {
  return (
    <div className="discover-places">
      <div className="container">
        <h1 className="PopularSearches__headline">
          Discover Venues<span> | By Event Type</span>
        </h1>
        <h2 className="PopularSearches__subheadline">
          Discover your perfect venues according to event type across the world
        </h2>
        <div className="explore-wrapper">
          <div className="stroke"></div>
          <div class="explore">
            <img src={ExploreIcon} alt="explore icon" />
            <span className="explore__text">Explore all</span>
          </div>
        </div>
        <div className="discover-places__list">
          {discoverPlaces.map(place => (
            <div
              style={{ backgroundImage: `url(${place.image})` }}
              className="discover-places__card"
            >
              <div className="discover-places__card-info">
                <span className="discover-places__card-name">{place.name}</span>
                <span className="discover-places__card-venues">
                  {place.venues} Venues
                </span>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DiscoverPlaces;
