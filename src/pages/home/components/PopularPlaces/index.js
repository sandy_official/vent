import React from "react";

import "./index.scss";

import PlaceCard from "../../../../components/PlaceCard";
import { Link } from "react-router-dom";
import Slider from "react-slick";

import Calefornia from "../../../../assets/popular/california.png";
import Florida from "../../../../assets/popular/florida.png";
import Texas from "../../../../assets/popular/texas.png";
import Virginia from "../../../../assets/popular/virginia.png";
import NewJersey from "../../../../assets/popular/newjersey.png";
import { connect } from "react-redux";
import { getLocation } from "../../../../redux/actions/places.actions/places.thunk";

const settings = {
    dots: false,
    infinite: false,
    speed: 700,
    slidesToShow: 5,
    slidesToScroll: 1
};

const popularPlaces = [
    {
        id: 1,
        name: "Calefornia",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Calefornia,
        venues: "150"
    },
    {
        id: 2,
        name: "Florida",
        text: "Lorem ipsum dolor sit amet",
        price: "500.00",
        per: "hour",
        persons: "1000-1500",
        rate: "4.2",
        image: Florida,
        venues: "25"
    },
    {
        id: 3,
        name: "Texas",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Texas,
        venues: "250"
    },
    {
        id: 4,
        name: "Virginia",
        text: "Lorem ipsum dolor sit amet",
        price: "100.00",
        per: "person",
        persons: "20-25",
        rate: "4.5",
        image: Virginia,
        venues: "75"
    },
    {
        id: 5,
        name: "New Jersey",
        text: "Lorem ipsum dolor sit amet",
        price: "600",
        per: "hour",
        persons: "1500-2000",
        rate: "4.5",
        image: NewJersey,
        venues: "500"
    },
    {
        id: 6,
        name: "Calefornia",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Calefornia,
        venues: "150"
    },
    {
        id: 7,
        name: "Florida",
        text: "Lorem ipsum dolor sit amet",
        price: "500.00",
        per: "hour",
        persons: "1000-1500",
        rate: "4.2",
        image: Florida,
        venues: "25"
    },
    {
        id: 8,
        name: "Texas",
        text: "Lorem ipsum dolor sit amet",
        price: "200.00",
        per: "person",
        persons: "40-50",
        rate: "4.5",
        image: Texas,
        venues: "250"
    }
];

class PopularPlaces extends React.PureComponent {
    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
    }

    componentDidMount() {
        this.props.getLocation();
    }

    next() {
        this.slider.slickNext();
    }

    previous() {
        this.slider.slickPrev();
    }

    render() {
        const { location }  = this.props;
        return (
            <div className="popular-places">
                <div className="container">
                    <h1 className="PopularSearches__headline">
        Popular Venues<span> | Across {location?.country_name}</span>
                    </h1>
                    <h2 className="PopularSearches__subheadline">
                        Our top rated venues for events across the {location?.country_name}
                    </h2>
                    <div className="stroke__wrapper">
                        <div className="stroke"></div>
                        <div className="slider__arrows">
                            <span className="prev-arrow" onClick={this.previous}></span>
                            <span className="next-arrow" onClick={this.next}></span>
                        </div>
                    </div>
                    <div className="PopularSearches__sliders">
                        {popularPlaces && (
                            <Slider ref={c => (this.slider = c)} {...settings}>
                                {popularPlaces.map((place, i) => (
                                    <Link
                                        key={place.id}
                                        to={`/place-details/${place.id}`}
                                        className="place-link"
                                    >
                                        <PlaceCard place={place} order={i}></PlaceCard>
                                    </Link>
                                ))}
                            </Slider>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    
    location: state.place.location
});

//export default PopularPlaces;
export default connect(mapStateToProps, {
    getLocation,
})(PopularPlaces);
