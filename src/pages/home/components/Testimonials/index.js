import React from "react";

import "./index.scss";

import Quotes from "../../../../assets/testimonials/quotes.png";
import Tesimonial1 from "../../../../assets/testimonials/testimonial-1.png";
import Tesimonial2 from "../../../../assets/testimonials/testimonial-2.png";
import Tesimonial3 from "../../../../assets/testimonials/testimonial-3.png";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const SlickButtonLeft = ({ currentSlide, slideCount, ...props }) => {
  const { className } = props;
  return <span {...props} className={className + " prev-arrow"}></span>;
};

const SlickButtonRight = ({ currentSlide, slideCount, ...props }) => {
  const { className } = props;
  return <span {...props} className={className + " next-arrow"}></span>;
};

const settings = {
  dots: false,
  infinite: false,
  speed: 700,
  prevArrow: <SlickButtonLeft />,
  nextArrow: <SlickButtonRight />,
  slidesToShow: 5,
  slidesToScroll: 1
};

const testimonials = [
  {
    image: Tesimonial1,
    name: "Alex Hornn",
    position: "Formar Manager Alex Bank"
  },
  {
    image: Tesimonial2,
    name: "Venta",
    position: "Formar Manager Alex Bank"
  },
  {
    image: Tesimonial3,
    name: "Bebby Mox",
    position: "Formar Manager Alex Bank"
  }
];

const Testimonials = () => {
  return (
    <div className="testimonials">
      <div className="container">
        <h1 className="PopularSearches__headline">
          Testimonials<span> | Customers’ Quotes</span>
        </h1>
        <h2 className="PopularSearches__subheadline">
          They’ve experienced , Now they’re talking about it.
        </h2>
        <div className="stroke"></div>
        <div className="testimonials__list">
          {testimonials.map(testimonial => (
            <div className="testimonials__card">
              <div className="testimonials__image">
                <img src={testimonial.image} alt={testimonial.name} />
              </div>
              <img
                className="testimonials__quotes"
                src={Quotes}
                alt="quotes icon"
              />
              <p className="etstimonials__text">
                Ontrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45 BC, making it over 2000 years old.
              </p>
              <div className="testimonials__stroke"></div>
              <span className="testimonials__name">{testimonial.name}</span>
              <span className="testimonials__position">
                {testimonial.position}
              </span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Testimonials;
