import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import PlaceCard from '../../../../components/PlaceCard';
import Pagination from 'react-js-pagination';

import './style.scss';

const DEFAULT_SKIP = 4;

class SearchList extends Component {
  state = {
    activePage: 1
  };

  handlePageChange = (pageNumber) => {
    const skip = DEFAULT_SKIP * (pageNumber - 1);
    this.setState({ activePage: pageNumber });
    this.props.getPlaces(skip);
  };

  render() {
    const { places } = this.props;

    const count = places.count;
    const placesList = places.rows;
    return (
      <div className="search-page-wrapper">
        <div className="search-page-wrapper__list">
          {placesList &&
            placesList.map((place, i) => (
              <Link key={place.id} to={`/place-details/${place.id}`} className="place-link">
                <PlaceCard key={`place_card_${i}`} place={place} />
              </Link>
            ))}
        </div>
        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={3}
          totalItemsCount={count}
          itemClass={'item-pagination'}
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}

export default SearchList;
