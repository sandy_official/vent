import React from 'react';
import { getSearchPlace } from '../../redux/actions/places.actions/searchPlace.thunk';
import { Container } from 'reactstrap';
import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { checkAuth } from '../../redux/actions/users.actions/auth.thunk';

import Header from '../../components/Header';
import SearchList from './components/SearchList/index';
import './components/MainBlock.scss';
import Filter from '../../components/Filter';
import history from '../../history';

class ListingPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.query = new URLSearchParams(this.props.location.search);

    this.state = {
      event: this.checkEventStatus(),
      location: this.query.get('formatted_address'),
      locationSelected: this.query.get('formatted_address'),
      startDate: this.query.get('date_from'),
      endDate: this.query.get('date_to'),
      guests: this.checkGuestsCount(),
      price: [this.query.get('price_min'), this.query.get('price_max')],
      skip: 0,
      limit: 4
    };
  }

  checkEventStatus = () => {
    const event = this.query.get('event');
    if (event === 'null' || !event) {
      return 'Please check event';
    }
    return event;
  };

  checkGuestsCount = () => {
    const guestsCount = this.query.get('guests');
    switch (guestsCount) {
      case 'small':
        return '1-30';
      case 'large':
        return '31-200';
      default:
        return '1-30';
    }
  };

  changeState(value, valueType) {
    this.setState(
      (state) => ({ ...state, [valueType]: value }),
      () => this.change()
    );
  }

  changeLocationState = (value) => {
    this.setState({ location: value });
  };

  onPlaceSelected = (place) => {
    const { formatted_address } = place;
    this.setState({ location: formatted_address, locationSelected: formatted_address }, () =>
      this.change()
    );
  };

  change() {
    const { event, locationSelected, startDate, endDate, guests, price } = this.state;
    let searchString = '?';

    if (locationSelected) {
      searchString += `formatted_address=${locationSelected}&`;
    }

    if (startDate) {
      searchString += `date_from=${startDate}&`;
    }

    if (endDate) {
      searchString += `date_to=${endDate}&`;
    }

    if (price) {
      searchString += `price_min=${price[0]}&price_max=${price[1]}&`;
    }

    if (guests === '1-30') {
      searchString += `guests=small&`;
    }

    if (guests === '31-200') {
      searchString += `guests=large&`;
    }

    if (event === 'Please check event') {
      searchString += 'event=null&';
    } else if (event) {
      searchString += `event=${event}&`;
    }

    history.replace({ pathname: '', search: searchString });
    this.props.getSearchPlace(this.state);
  }

  componentDidMount() {
    this.props.checkAuth();
    this.props.getSearchPlace(this.state);
  }

  onChangePaging = (skip) => {
    this.setState({ skip });
    this.props.getSearchPlace(this.state);
    // this.props.getSearchPlace(skip);
  };

  render() {
    const { isAuthenticated, places } = this.props;
    const { event, location, startDate, endDate, guests, price } = this.state;

    return (
      <div className="wrapper">
        <div className="container">
          <Header isAuthenticated={isAuthenticated} find={true}></Header>
        </div>
        <Filter
          onChangeFilters={(value, valueType) => this.changeState(value, valueType)}
          onPlaceSelected={this.onPlaceSelected}
          changeLocationState={this.changeLocationState}
          event={event}
          startDate={startDate}
          endDate={endDate}
          guests={guests}
          price={price}
          location={location}
        />
        <Container>
          <SearchList places={places} getPlaces={this.onChangePaging} />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.searchPlace.place.filteredBuildingsInfo,
    loading: state.searchPlace.place.searchPlaceLoading,
    error: state.searchPlace.place.searchPlaceLoading,
    places: state.searchPlace.places,
    isAuthenticated: state.auth.isAuthenticated,
    checkAuthError: state.auth.checkAuthError,
    loginError: state.auth.loginError
  };
};

const ListingPageWithRouter = withRouter(ListingPage);

export default connect(mapStateToProps, {
  getSearchPlace,
  checkAuth
})(ListingPageWithRouter);
