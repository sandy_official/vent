import React from 'react';
import { Container } from 'reactstrap';
import Rating from '@material-ui/lab/Rating';
import { withRouter } from 'react-router-dom';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import moment from 'moment';

import history from '../../../history';
import Header from '../../../components/Header';
import { AirbnbRangeDatesPicker } from '../../../components/styledComp/AirbnbRangeDatesPicker';
import TimePiker from '../../../components/TimePicker';
import view from '../../../assets/view.png';
import location from '../../../assets/location.svg';
import user from '../../../assets/user.svg';
import event from '../../../assets/event.svg';
import guest from '../../../assets/guest.svg';
import Footer from '../../../components/Footer';
import Attendees from '../../../components/Filter/Attendees';
import Event from '../../../components/Filter/Event';
import {
  changeActivity,
  changeGuests,
  setBookingDatesTime
} from '../../../redux/actions/booking.actions/booking.actions';

import './styles.scss';

class RequestBookPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startTime: '00:00',
      endTime: '23:59',
      showSTime: false,
      showETime: false,
      activity: 'Choose the activity'
    };
  }

  componentDidMount() {
    setBookingDatesTime(this.props.parameters);
  }

  changeSTime(newTime) {
    this.setState({ startTime: newTime });
  }

  changeETime(newTime) {
    this.setState({ endTime: newTime });
  }

  changeShowSTime = (value) => {
    if (!this.state.showETime) {
      this.setState({ showSTime: value });
    }
  };

  changeShowETime = (value) => {
    if (!this.state.showSTime) {
      this.setState({ showETime: value });
    }
  };
  liftUpDates = (fromDate, toDate) => {
    this.setState({
      date_from: fromDate,
      date_to: toDate
    });
  };

  submitForm = () => {};

  renderDates = () => {
    const { startDate, endDate } = this.props.parameters;
    return `${startDate.format('ll')} – ${endDate.format('ll')}`;
  };

  getHours = () => {
    const { startDate, endDate, startTime, endTime } = this.props.parameters;
    const sTime = startDate.clone().set('hour', startTime.substr(0, 2));
    const eTime = endDate.clone().set('hour', endTime.substr(0, 2));
    console.log();
    return eTime.diff(sTime, 'hours');
  };

  render() {
    const { parameters, changeActivity, changeGuests, isAuthenticated } = this.props;
    const { showSTime, showETime } = this.state;
    return !parameters ? (
      <Redirect to="/home" />
    ) : (
      <div className="wrapper">
        <Container>
          <Header isAuthenticated={isAuthenticated} find={true} />
          <div className="booking-page">
            <div className="booking-page__content">
              <h1 className="booking-page__title">Request to book</h1>
              <h2 className="booking-page__subtitle">Booking details</h2>
              <div className="booking-page__parameter">
                <h4 className="booking-page__parameter-header">Date</h4>
                <AirbnbRangeDatesPicker
                  liftUpDates={this.liftUpDates}
                  startDate={parameters.startDate}
                  endDate={parameters.endDate}
                />
              </div>
              <div className="booking-page__parameter">
                <h4 className="booking-page__parameter-header">Time</h4>
                <div className="booking-page__time">
                  <TimePiker
                    time={parameters.startTime}
                    setTime={(value) => this.changeSTime(value)}
                    showTime={showSTime}
                    setShowTime={(value) => this.changeShowSTime(value)}
                  />
                  <TimePiker
                    time={parameters.endTime}
                    setTime={(value) => this.changeETime(value)}
                    showTime={showETime}
                    setShowTime={(value) => this.changeShowETime(value)}
                  />
                </div>
              </div>
              <div className="booking-page__parameter">
                <h4 className="booking-page__parameter-header">Activity</h4>
                <div className="booking-page__input">
                  <Event
                    changeState={(value) => {
                      this.setState({ activity: value });
                      changeActivity(value);
                    }}
                    event={this.state.activity}
                  />
                </div>
              </div>
              <div className="booking-page__parameter">
                <h4 className="booking-page__parameter-header">How many people will attend</h4>
                <div className="booking-page__input">
                  <Attendees
                    changeState={(value) => changeGuests(value)}
                    guests={parameters.guest.count}
                  />
                </div>
              </div>
              <div className="booking-page__parameter">
                <h4 className="booking-page__parameter-header">Message to your host</h4>
                <div className="booking-page__input booking-page__input_textarea">
                  <textarea className="booking-page__input-field booking-page__input-field_textarea" />
                </div>
              </div>
            </div>
            <div className="booking-page__sidebar">
              <div className="booking-page__sidebar-section booking-page__sidebar-section_view">
                <div className="booking-page__section-content">
                  <h4 className="booking-page__parameter-header">
                    Beautiful new central apartment
                  </h4>
                  <div className="booking-page__reviews">
                    <div className="booking-page__stars">
                      <Rating name="read-only" value={4} readOnly />
                    </div>
                    <div className="booking-page__counter">100 reviews</div>
                  </div>
                </div>
                <div className="booking-page__view">
                  <img className="booking-page__img" src={view} alt="Building view" />
                </div>
              </div>
              <div className="booking-page__sidebar-section">
                <div className="booking-page__location">
                  <div className="booking-page__location-img">
                    <img src={location} alt="Location icon" />
                  </div>
                  <div className="booking-page__place">USA, Santa Clara</div>
                </div>
                <div className="booking-page__host">
                  <div className="booking-page__host-content">
                    <h4 className="booking-page__parameter-header">Host: Name</h4>
                    <div className="booking-page__host-name">&#129351; Super owner</div>
                  </div>
                  <div className="booking-page__host-img">
                    <img className="booking-page__host-icon" src={user} alt="User icon" />
                  </div>
                </div>
              </div>
              <div className="booking-page__sidebar-section">
                <div className="booking-page__params booking-page__params_start">
                  <div className="booking-page__param-img">
                    <img className="booking-page__param-icon" src={guest} alt="Guest icon" />
                  </div>
                  <div className="booking-page__param-text">{`${parameters.guest.count} people`}</div>
                </div>
                <div className="booking-page__params booking-page__params_start">
                  <div className="booking-page__param-img">
                    <img className="booking-page__param-icon" src={event} alt="Event icon" />
                  </div>
                  <div className="booking-page__param-text">{this.renderDates()}</div>
                </div>
                <div className="booking-page__params">
                  <div className="booking-page__param-text">{`$150 x ${this.getHours()}`} </div>
                  <div className="booking-page__param-text">{this.getHours() * 150} $</div>
                </div>
                <div className="booking-page__params">
                  <div className="booking-page__param-text">Service fee</div>
                  <div className="booking-page__param-text">$75</div>
                </div>
              </div>
              <div className="booking-page__total">
                <span className="booking-page__param-text booking-page__param-text_bold">
                  Total (USD)
                </span>
                <span className="booking-page__param-text booking-page__param-text_bold">
                  {this.getHours() * 150 + 75} $
                </span>
              </div>
            </div>
          </div>
          <div className="booking-page__buttons">
            <div className="booking-page__send-request">
              <button className="send-request-btn" onClick={this.submitForm}>
                Send a message
              </button>
            </div>
            <div className="booking-page__send-request">
              <button className="send-request-btn" onClick={() => history.push('/payment')}>
                Confirm
              </button>
            </div>
          </div>
          <Footer />
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  const { booking } = state;
  return {
    parameters: booking.parameters,
    isAuthenticated: state.auth.isAuthenticated
  };
};

const requestBookPageWithRouter = withRouter(RequestBookPage);

export default connect(mapStateToProps, {
  changeActivity,
  changeGuests,
  setBookingDatesTime
})(requestBookPageWithRouter);
