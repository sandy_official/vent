import React from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { Form } from 'reactstrap';

import { uploadFiles } from '../../../redux/actions/places.actions/places.thunk';
import { getPlaceDetails } from '../../../redux/actions/places.actions/editPlace.thunk';

import styled from 'styled-components';

const PageTitle = styled.h2`
  font-weight: 800;
  font-size: 36px;

  > span {
    opacity: 0.5;
  }
`;

class UploadFiles extends React.PureComponent {
  fileInput = React.createRef();

  state = {
    file: null,
    filename: '',
    fileHashed: ''
  };

  componentDidMount() {
    this.props.getPlaceDetails(this.props.match.params.id);
  }

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  renderInput = ({ input, label, meta, type }) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    return (
      <div className={className}>
        <label>{label} </label>
        <input {...input} autoComplete="off" type={type} />
        {this.renderError(meta)}
      </div>
    );
  };

  uploadFilesHandler = async (e, method) => {
    e.preventDefault();
    const { filename, fileHashed } = this.state;
    this.props.uploadFiles(filename, fileHashed, this.props.place.id);
    // if (method === 'multer') {

    // let imageFormObj = new FormData();

    // imageFormObj.append('file', this.state.file);
    // for (const pair of imageFormObj.entries()) {
    // imageFormObj.append(pair[0], pair[1]);
    // }
    // if (window.FileReader) {
    // const toBase64 = () => {
    //   const reader = new FileReader();
    //   reader.readAsDataURL(this.state.file);
    //   reader.onload = () => resolve(reader.result);
    //   reader.onloadend = () => {
    // this.props.uploadFiles(this.state.fileHashed, this.props.place.id);
    //    };
    //   };
    // };
    // }
  };

  onChooseImg = async () => {
    const file = this.fileInput.current.files[0];

    const toBase64 = (file) =>
      new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);
      });
    const fileHashed = await toBase64(file);
    this.setState(() => ({
      file,
      filename: file.name,
      fileHashed
    }));
  };

  render() {
    const { file } = this.state;
    return (
      <>
        <PageTitle>
          Venue Info <span>· Part 2</span>
        </PageTitle>
        <Form onSubmit={(e) => this.uploadFilesHandler(e, 'multer')}>
          <input
            type="file"
            name="files"
            onChange={this.onChooseImg}
            label="Add an image"
            ref={this.fileInput}
          />
          <button onClick={(e) => this.uploadFilesHandler(e, 'multer')}>Upload</button>
        </Form>
        <br />
        {file && <img src={this.state.fileHashed} style={{ maxWidth: '300px' }} alt="img" />}
      </>
    );
  }
}

const formWrapped = reduxForm({
  form: 'Files'
})(UploadFiles);

const mapStateToProps = (state) => ({
  place: state.editPlace.place.building,
  loading: state.place.uploadFilesLoading,
  error: state.place.uploadFilesError
});

export default connect(mapStateToProps, { uploadFiles, getPlaceDetails })(formWrapped);
