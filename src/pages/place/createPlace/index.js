import React from "react";
import { connect } from "react-redux";
import { getPlaceDetails } from "../../../redux/actions/places.actions/places.thunk";
import { isAuthenticated } from "../../../redux/selectors/auth/auth.select";
import Header from "../../../components/Header";
import Footer from "../../../components/Footer";
import "./style.scss";
import BasicInfo from "./basicinfo/basicinfo";
import AddMedia from "./addmedia/add-media";

const mapStateToProps = state => {
  return {
    isAuthenticated: isAuthenticated(state)
  };
};

class CreatePlace extends React.PureComponent {
  state = {
    details: null,
    isOpen: false,
    step: 1
  };

  getStep = step => {
    this.setState({
      step: step
    });
  };

  render() {
    const { isAuthenticated } = this.props;
    return (
      <div className="create-place">
        {/* <div className="create-place-header-section">
          <Header isAuthenticated={isAuthenticated} find={true} />
        </div> */}
        <main className="create-place-container">
          <div className="create-place-container-header">
            Add a Venue
            <span className="create-place-container-header-border"></span>
            <span
              className={
                "create-place-container-header-stepsection " +
                (this.state.step === 1 ? "active" : "")
              }
            >
              Step 1
            </span>
            <span className="create-place-container-header-stepsection">
              {" "}
              &nbsp; &gt; &nbsp;{" "}
            </span>
            <span
              className={
                "create-place-container-header-stepsection " +
                (this.state.step === 2 ? "active" : "")
              }
            >
              Step 2
            </span>
            {/* <span className="create-place-container-header-stepsection">
              {" "}
              &nbsp; &gt; &nbsp;{" "}
            </span>
            <span
              className={
                "create-place-container-header-stepsection " +
                (this.state.step === 3 ? "active" : "")
              }
            >
              Step 3
            </span> */}
          </div>
          <div className="create-place-container-subheader">
            Fill quick information about venue
          </div>
          <div className="create-place-container-venuesection">
            <BasicInfo step={this.getStep} />
            {/* <AddMedia /> */}
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}

export default connect(mapStateToProps, { getPlaceDetails })(CreatePlace);
