import React from "react";
import { connect } from "react-redux";
import {
  getPlaceDetails,
  uploadMedia
} from "../../../../redux/actions/places.actions/places.thunk";
import { isAuthenticated } from "../../../../redux/selectors/auth/auth.select";
import "./style.scss";
import Dropdown from "../../../../components/Dropdown";
import ReactPlayer from "react-player";
import PhotoDropzone from "../../../../components/PhotoDropzone";
import Dropzone from "react-dropzone";
import Loader from "../../../../assets/icons/tenor.gif";

const mapStateToProps = state => {
  return {
    isAuthenticated: isAuthenticated(state),
    file: state.place.files
  };
};

class AddMedia extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  state = {
    details: null,
    isOpen: false,
    media: [],
    feature: "Select",
    selectedFeatures: [],
    addedFeatures: {
      alcohol: false,
      smocking: false,
      music: false,
      pets: false,
      instruments: false
    },
    fileUploading: false,
    percentage: "",
    filesLength: 0,
    filesProgress: 0
  };

  componentDidUpdate() {
    // const { file } = this.props;
    // let { media } = this.state;
    // if(file && file.Location) {
    //     let idx = media.findIndex((rec) => {
    //         if(file.Location === rec.Location) {
    //             return file;
    //         }
    //     });
    //if(idx === -1) {
    // let { filesProgress } = this.state;
    // filesProgress++;
    // this.setState({
    //     filesProgress: filesProgress,
    //     fileUploading: this.state.filesLength === filesProgress ? false: true
    // });
    //}
    // }
    // const { file } = this.props;
    // let { media } = this.state;
    // if(file && file.Location) {
    //     media.push(file);
    // }
    // this.setState({ media: media })
  }

  dropFiles = acceptedFiles => {
    this.setState({
      filesLength: acceptedFiles.length + this.state.filesLength
    });

    acceptedFiles.forEach((file, index) => {
      const formData = new FormData();
      const filetype = file.type.split("/")[0];
      filetype === "image"
        ? formData.append("images", file)
        : formData.append("videos", file);
      this.getFiles(formData, filetype);
      // const xhr = new XMLHttpRequest();
      // xhr.open('POST', `https://ventapi.customerdemourl.com/api/buildings/upload-media/${filetype}`, true);
      // xhr.setRequestHeader("Authorization", localStorage.getItem('token'));
      // xhr.send(formData);

      // file.uploading = true;
      // file.percentage = 0;
      // let files = this.state.files;
      // files.push(file);
      // this.setState({
      //     files: files
      // })
      // //xhr.addEventListener("progress", this.handleProgress);
      // xhr.onprogress = event => {
      //     console.log(event);
      //     const percentage = +((event.loaded / event.total) * 100).toFixed(2);
      //     files[index].percentage = percentage;
      //     this.setState({ files: files });
      // };
      // xhr.onreadystatechange = () => {
      //     if (xhr.readyState !== 4) return;
      //     if (xhr.status !== 200) {
      //     /*handle error*/
      //     }

      //     if(xhr.status === 200 || xhr.status === 201) {
      //         files[index].uploading = false;
      //         files[index].response = JSON.parse(xhr.response);
      //         this.setState({ files: files });
      //     }
      // };
    });
  };

  getFiles = (formData, type) => {
    this.setState({
      fileUploading: true
    });

    if (type === "image") {
      this.props.uploadMedia(formData, "images");
    } else {
      this.props.uploadMedia(formData, "videos");
    }
    this.forceUpdate();
    //this.setState({ upload: true, mediaArr: [...this.state.mediaArr, ...file] });
  };

  changeItemActivities = item => {
    let { selectedFeatures } = this.state;
    if (this.state.selectedFeatures.indexOf(item) === -1) {
      selectedFeatures.push(item);
    }
    this.setState({ feature: item, selectedFeatures: selectedFeatures });
  };

  addFeature = () => {
    const { selectedFeatures } = this.state;
    const features = {
      alcohol: selectedFeatures.indexOf("Alcohol") != -1 ? true : false,
      smocking: selectedFeatures.indexOf("Smoking") != -1 ? true : false,
      music: selectedFeatures.indexOf("Music") != -1 ? true : false,
      pets: selectedFeatures.indexOf("Pets") != -1 ? true : false,
      instruments: selectedFeatures.indexOf("Instruments") != -1 ? true : false
    };
    this.setState({
      addedFeatures: features
    });
    this.props.featuresData(this.state.addedFeatures);
  };

  removeActivity = index => {
    console.log(index);
  };

  saveMedia = () => {
    this.props.mediaData(this.state.media);
    this.props.featuresData(this.state.addedFeatures);
    this.props.step(2);
  };

  render() {
    const { file } = this.props;
    let { media, feature, addedFeatures, fileUploading } = this.state;

    if (file && file.Location) {
      let idx = media.findIndex(rec => {
        if (file.Location === rec.Location) {
          return file;
        }
      });
      if (idx === -1) {
        media.push(file);
      }
      // let { filesProgress } = this.state;
      // filesProgress++;
      this.setState({
        // filesProgress: filesProgress,
        fileUploading: this.state.filesLength === media.length ? false : true
      });
    }
    this.setState({ media: media });

    return (
      <div className="add-media">
        <div className="row">
          <div className="col-md-6 add-media-borderpart">
            <div className="create-place-container-venuesection-heading">
              Add Photos / Videos
            </div>
            <div className="add-media-text">
              Please upload jpg or png files with each photo a max 5MB per photo
              ( Max 6) and upload video mp3 files with each video a Max of 60
              seconds
            </div>
            <div className="row">
              {media.map((item, i) => (
                <div className="col-md-6 mb-20" key={i}>
                  <div className="add-media-imgcontainer">
                    {/* <img src={item.Location} /> */}
                    {item.type === 1 ? (
                      <img src={item.Location} />
                    ) : (
                      <ReactPlayer
                        className="video-item"
                        width="100%"
                        height="100%"
                        controls={true}
                        url={item.Location}
                        playing={false}
                        config={{
                          youtube: {
                            preload: false
                          }
                        }}
                      />
                    )}
                    <span className="add-media-crossicon">x</span>
                  </div>
                </div>
              ))}

              <div className="col-md-6 mb-20">
                {/* {fileUploading ? (<div className="uploading"><PhotoDropzone type="venue" getFiles={this.getFiles} /></div>) : (<PhotoDropzone type="venue" getFiles={this.getFiles} />)} */}
                <Dropzone onDrop={this.dropFiles} accept="image/*, video/*">
                  {({ getRootProps, getInputProps }) => (
                    <div className="add-media-uploadedsection">
                      {fileUploading ? (
                        <div className="uploading">
                          <img src={Loader} />
                        </div>
                      ) : (
                        ""
                      )}
                      <div {...getRootProps({ className: "dropzone-window" })}>
                        <input {...getInputProps()} />
                        <p>
                          Drag files here or{" "}
                          <span className="browse-word">browse</span> to upload
                        </p>
                      </div>
                    </div>
                  )}
                </Dropzone>
              </div>
            </div>
          </div>
          <div className="col-md-6 add-media-borderpartleft">
            <div className="create-place-container-venuesection-heading">
              Activities allowed on the venue
            </div>
            <div className="add-media-text">
              select and you can add as meny as you want
            </div>
            <div className="row">
              <div className="col-md-6 ddlWrapper">
                <Dropdown
                  changeItem={this.changeItemActivities}
                  item={feature}
                  list={["Alcohol", "Smoking", "Music", "Pets", "Instruments"]}
                />
              </div>
              <div className="col-md-6">
                <button
                  onClick={this.addFeature}
                  className="btn-venue add-media-venuebtn mt-0"
                >
                  Add
                </button>
              </div>
            </div>
            <div className="add-media-borderbottom"></div>
            <div className="row">
              <div className="col-md-12">
                {Object.keys(addedFeatures).map(key =>
                  addedFeatures[key] == true ? (
                    <div className="add-media-selectoption" key={key}>
                      {key == "smocking" ? "Smoking" : key}
                      <div
                        onClick={this.removeActivity.bind(key)}
                        className="add-media-selectoption-crossicon"
                      >
                        x
                      </div>
                    </div>
                  ) : (
                    <></>
                  )
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 text-center">
            <button onClick={this.saveMedia} className="btn-venue">
              Next
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, { getPlaceDetails, uploadMedia })(
  AddMedia
);
