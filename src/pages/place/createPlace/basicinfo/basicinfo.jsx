import React from "react";
import { connect } from "react-redux";
import { Form } from "reactstrap";
import {
  categoriesList,
  venueCapacities,
  venueAreas,
  createPlaces,
  getCurrentExchangeRate,
  getLocation
} from "../../../../redux/actions/places.actions/places.thunk";
import { isAuthenticated } from "../../../../redux/selectors/auth/auth.select";
import "./style.scss";
import InputControl from "../../../../components/InputControls/InputControl";
import TextAreaControl from "../../../../components/TextArea";
import CustomDropdown from "../../../../components/Dropdown/CustomDropdown";
import MultiselectInputAddOn from "../../../../components/InputControls/SelecteInputAddOn/multiselectInputAddOn";
import AddMedia from "../addmedia/add-media";
import AddAmenities from "../addamenities/add-amenities";
import styled from "styled-components";
import swal from "sweetalert";

// GOOGLE
import Autocomplete from "react-google-autocomplete";
// import { latest } from "immer/dist/common";
// map View
import MapContainer from "../../../../components/MapView/mapComponent";
import AddressExtracter from "../../../../components/MapView/addressExtractor";

const AutocompleteStyled = styled(Autocomplete)`
  border: solid 1px #e9e9e9;
  height: 40px;
  display: block;
  width: 100%;
  padding: 6px 12px;
  border-radius: 5px;
  font-size: 13px;
  font-weight: 300;
  line-height: normal;
  letter-spacing: normal;
  color: #a5a5a5;
`;

class BasicInfo extends React.PureComponent {
  state = {
    details: null,
    isOpen: false,
    venueType: "Select",
    venue: { id: "", name: "Select" },
    capacityType: "Select",
    capacity: { id: "", name: "Select" },
    areaType: "Select",
    area: { id: "", name: "Select" },
    name: "",
    rate: "",
    venueAddress: "",
    formatted_address: "",
    venueState: "",
    city: "",
    zipCode: "",
    lat: "",
    lng: "",
    country: "",
    description: "",
    rateTimeItem: "Hourly",
    rateExcItem: "EUR",
    exchangeRatelist: [],
    features: {},
    images: [],
    amenities: [],
    specialFeatures: [],
    operatingHours: [],
    rules: "",
    step: 1,
    dropdownStatus: [
      { name: "currency", status: false },
      { name: "chargeType", status: false },
      { name: "area", status: false },
      { name: "vanueType", status: false },
      { name: "capacity", status: false }
    ],
    random: false,
    coordinates: { lat: 38.09, lng: -101.71 },
    venueTypeStatus: false,
    capacityStatus: false,
    areaStatus: false,
    currencyStatus: false,
    chargeTypeStatus: false
  };

  componentDidMount() {
    this.props.getLocation();
    this.props.getCurrentExchangeRate();
    this.props.categoriesList();
    this.props.venueCapacities();
    this.props.venueAreas();
  }

  componentDidUpdate() {
    const { allRates, saveVenue } = this.props;
    const { exchangeRatelist, coordinates } = this.state;
    if (allRates && exchangeRatelist.length === 0) {
      this.handleExchangeData();
    }
    if (saveVenue && coordinates.lat == 38.09 && coordinates.lng == -101.71) {
      this.setCurrentLocation();
    }
  }

  handleExchangeData = () => {
    const { allRates } = this.props;
    const { exchangeRatelist } = this.state;
    let keys = Object.keys(allRates);
    if (exchangeRatelist.length < 1) this.setState({ exchangeRatelist: keys });
  };

  setCurrentLocation = () => {
    const { saveVenue } = this.props;
    const { coordinates } = this.state;
    // console.log("Locaion from props-->,",saveVenue)
    let tmpObj = {
      lat: saveVenue.location.latitude,
      lng: saveVenue.location.longitude
    };
    this.setState({ coordinates: tmpObj });
  };

  submitVenue = submit => {
    if (submit) {
      //   debugger;
      const {
        name,
        venueType,
        rateTimeItem,
        capacityType,
        rate,
        areaType,
        venueAddress,
        city,
        venueState,
        zipCode,
        description,
        images,
        amenities,
        specialFeatures,
        operatingHours,
        rules,
        country,
        lat,
        lng,
        formatted_address,
        features
      } = this.state;

      const zipData = {
        zip_code: zipCode,
        street: venueAddress,
        floor: 1
      };
      const data = {
        zipCode: zipData,
        country_name: country,
        city_name: city,
        UserId: localStorage.getItem("id"),
        amenities: amenities,
        description: description,
        formatted_address: formatted_address,
        guests: capacityType,
        mediaLinks: [],
        name: name,
        operatingHours: operatingHours,
        price: rate,
        rules: features,
        specialFeatures: specialFeatures,
        square: areaType,
        zipCode: zipData,
        lat: lat,
        lon: lng,
        renter_email: "",
        category_id: venueType,
        images: images,
        rulemsg: rules,
        priceType: rateTimeItem
      };
      this.props.createPlaces(data);
      swal("Sucess", "Venue created successfully.", "success").then(() => {
        window.location.href = "/home";
      });
      //alert('Venue created successfully.');
    }
  };

  changeVenueName = e => {
    this.setState({ name: e.target.value });
  };

  changeItemVenueType = item => {
    this.setState({ venueType: item.id, venue: item });
  };

  rateTimeChange = item => {
    this.setState({ rateTimeItem: item });
  };

  currencyRateChange = item => {
    this.setState({ rateExcItem: item });
  };

  changeItemCapacity = item => {
    this.setState({ capacityType: item.id, capacity: item });
  };

  changeItemRate = e => {
    this.setState({ rate: e.target.value });
  };

  disableDropdown = param => {
    console.log("Params for disabling-->", param);
  };

  // clickDropDown=(entity)=>{
  //  const {dropdownStatus} = this.state;
  //  let ind = dropdownStatus.findIndex((ele)=>ele.name == entity)
  //  let flag = dropdownStatus[ind].status
  //  dropdownStatus[ind].status= !flag;
  // }

  handleOutSide = type => {
    this.setState({
      areaStatus: false,
      venueTypeStatus: false,
      capacityStatus: false,
      currencyStatus: false,
      chargeTypeStatus: false
    });
  };

  onClickHandle = type => {
    const {
      areaStatus,
      venueTypeStatus,
      capacityStatus,
      currencyStatus,
      chargeTypeStatus
    } = this.state;
    switch (type) {
      case "currency":
        this.setState({
          areaStatus: false,
          venueTypeStatus: false,
          capacityStatus: false,
          currencyStatus: !currencyStatus,
          chargeTypeStatus: false
        });
        break;
      case "chargeType":
        this.setState({
          areaStatus: false,
          venueTypeStatus: false,
          capacityStatus: false,
          currencyStatus: false,
          chargeTypeStatus: !chargeTypeStatus
        });
        break;
      case "area":
        this.setState({
          areaStatus: !areaStatus,
          venueTypeStatus: false,
          capacityStatus: false,
          currencyStatus: false,
          chargeTypeStatus: false
        });
        break;
      case "capacity":
        this.setState({
          areaStatus: false,
          venueTypeStatus: false,
          capacityStatus: !capacityStatus,
          currencyStatus: false,
          chargeTypeStatus: false
        });
        break;
      case "vanueType":
        this.setState({
          areaStatus: false,
          venueTypeStatus: !venueTypeStatus,
          capacityStatus: false,
          currencyStatus: false,
          chargeTypeStatus: false
        });
        break;

      default:
        this.setState({
          //  areaStatus: false,
          // venueTypeStatus: false,
          // capacityStatus: false,
          // currencyStatus: false,
          // chargeTypeStatus: false
        });
    }
  };

  openStatus = data => {
    const { dropdownStatus } = this.state;
    if (data) {
      dropdownStatus.forEach(
        (ele, ind) => {
          if (data.status) {
            if (ele.name !== data.name) {
              ele.status = false;
            } else {
              ele.status = true;
            }
          } else {
            ele.status = false;
          }
        },
        () => {
          this.setState({
            random: !this.state.random
          });
        }
      );
    }

    console.log("Drop Down Status @ page ", data, dropdownStatus);
  };

  changeItemArea = item => {
    this.setState({ areaType: item.id, area: item });
  };

  changeVenueAddress = e => {
    this.setState({ venueAddress: e.target.value });
  };

  changeCityInput = event => {
    this.setState({ city: event.target.value });
  };

  changeStateInput = event => {
    this.setState({ venueState: event.target.value });
  };

  changeZipCode = event => {
    this.setState({ zipCode: event.target.value });
  };

  changeMessage = event => {
    this.setState({ description: event.target.value });
  };

  handleSubmit = e => {
    console.log("Handle Submit Called -->");
    e.preventDefault();
    this.setState({
      step: 2
    });
    this.props.step(this.state.step);
  };

  onPlaceSelected = async place => {
    // debugger;
    const { formatted_address } = place;
    const lat = place.geometry.location.lat();
    const lng = place.geometry.location.lng();

    let state_code = formatted_address.split(",")[1];
    if (state_code.includes(" ")) {
      state_code = state_code.split(" ")[1];
    }
    var state_name = "";
    place.address_components.forEach(rec => {
      if (rec.short_name == state_code) {
        state_name = rec.long_name;
      }
    });
    const city_name = formatted_address.split(",")[0];
    const country_name = formatted_address.split(",")[2].split(" ")[1];
    this.setState({
      formatted_address: formatted_address,
      city: city_name,
      venueState: state_name,
      lat: lat,
      lng: lng,
      country: country_name
    });
  };

  getFeatures = features => {
    this.setState({ features: features });
  };

  getMedia = media => {
    if (media.length !== 0) {
      //   debugger;
      let files = [];
      media.forEach(file => {
        files.push({
          type: file.type,
          link: file.Location
        });
      });
      this.setState({ images: files });
    }
  };

  getStep = step => {
    console.log("Get STep==>", step);
    this.setState({
      step: step
    });
    this.props.step(step);
  };

  getAmenities = amenities => {
    // debugger;
    this.setState({
      amenities: amenities
    });
  };

  getSpecialFeatures = features => {
    this.setState({
      specialFeatures: features
    });
  };

  getOperatingHours = hours => {
    this.setState({
      operatingHours: hours
    });
  };

  getRules = rules => {
    this.setState({
      rules: rules
    });
  };
  onPositionChange = e => {
    // console.log("Event=-->", e);
    const { coordinates, venueAddress } = this.state;
    this.setState({
      coordinates: e.center
    });
    AddressExtracter(coordinates.lat, coordinates.lng).then(res => {
      // console.log("response -->", res);

      if (res && res.status === "Success") {
        const {
          city1,
          country,
          locality,
          postal_code,
          state,
          street_number
        } = res.data.CustomAddress;
        if (typeof (parseInt(street_number) === "number")) {
          this.setState({
            venueAddress: street_number + "," + city1
          });
        }
        this.setState({
          venueAddress: city1,
          zipCode: postal_code,
          venueState: state,
          city: locality
        });
      }
    });
  };

  UNSAFE_componentWillReceiveProps = props => {
    let location = {
      lat: props.saveVenue.location.latitude,
      lng: props.saveVenue.location.longitude
    };

    if (location !== this.state.coordinates) {
      this.setState({ coordinates: location });
    }
  };

  render() {
    const { categories, capacities, areas } = this.props;
    const {
      name,
      rate,
      zipCode,
      venueState,
      venueAddress,
      description,
      area,
      venue,
      capacity,
      rateTimeItem,
      rateExcItem,
      exchangeRatelist,
      city,
      dropdownStatus,
      currencyStatus,
      chargeTypeStatus,
      venueTypeStatus,
      areaStatus,
      capacityStatus
    } = this.state;
    return (
      <div className="create-place-container-venuesection-baseClass">
        {this.state.step === 1 ? (
          <>
            <div className="create-place-container-venuesection-baseClass-heading">
              About Venue
            </div>
            <Form onSubmit={this.handleSubmit} className="form-block__form">
              <div className="row">
                <div className="col-md-6">
                  <InputControl
                    placeHolder="Enter name of venue"
                    value={name}
                    onTextChange={this.changeVenueName}
                    label="Venue Name"
                  />
                  <div className="row">
                    <div className="col-md-6">
                      <MultiselectInputAddOn
                        onTextChange={this.changeItemRate}
                        value={rate}
                        label="Rates"
                        placeHolder="Enter"
                        onDropDownChange={this.rateTimeChange}
                        item={rateTimeItem}
                        listOfItem={["Hourly", "Per Day"]}
                        RateItem={rateExcItem}
                        listOfAllRates={exchangeRatelist}
                        onCurrencyDropDownChange={this.currencyRateChange}
                        openStatus={this.openStatus}
                        // enableThroughPropCurrency={dropdownStatus[0].status}
                        // enableThroughPropChargeType={dropdownStatus[1].status}
                        currencyStatus={currencyStatus}
                        chargeTypeStatus={chargeTypeStatus}
                        currencyOnClickHandleOutside={() =>
                          this.handleOutSide("currency")
                        }
                        currencyOnClickHandle={() =>
                          this.onClickHandle("currency")
                        }
                        chargeTypeOnClickHandleOutside={() =>
                          this.handleOutSide("chargeType")
                        }
                        chargeTypeOnClickHandle={() =>
                          this.onClickHandle("chargeType")
                        }
                      ></MultiselectInputAddOn>
                    </div>

                    <div className="col-md-6 ddlWrapper">
                      <span>Area ( Square Feet )</span>

                      <CustomDropdown
                        changeItem={this.changeItemArea}
                        item={area}
                        list={areas}
                        status={areaStatus}
                        type={"object"}
                        customRef="area"
                        openStatus={this.openStatus}
                        onClickHandleOutside={() => this.handleOutSide("area")}
                        onClickHandle={() => this.onClickHandle("area")}
                        enableThroughProp={dropdownStatus[2].status}
                        // enableThroughProp={true}
                      />
                    </div>
                  </div>
                  <TextAreaControl
                    placeHolder="Enter description"
                    value={description}
                    changeText={this.changeMessage}
                    label="Description"
                  />
                </div>
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-6 ddlWrapper">
                      <span>venue type</span>

                      <CustomDropdown
                        changeItem={this.changeItemVenueType}
                        item={venue}
                        list={categories}
                        type={"object"}
                        status={venueTypeStatus}
                        customRef="venueType"
                        openStatus={this.openStatus}
                        onClickHandleOutside={() =>
                          this.handleOutSide("vanueType")
                        }
                        onClickHandle={() => this.onClickHandle("vanueType")}
                        // enableThroughProp={dropdownStatus[3].status}
                        enableThroughProp={false}
                      />
                    </div>
                    <div className="col-md-6 ddlWrapper">
                      <span>CAPACITY</span>
                      <CustomDropdown
                        changeItem={this.changeItemCapacity}
                        item={capacity}
                        list={capacities}
                        type={"object"}
                        status={capacityStatus}
                        customRef="capacity"
                        openStatus={this.openStatus}
                        onClickHandle={() => this.onClickHandle("capacity")}
                        onClickHandleOutside={() =>
                          this.handleOutSide("capacity")
                        }
                        // onClickHandleOutside={this.handleOutSide('capacity')}
                        // enableThroughProp={dropdownStatus[4].status}
                        enableThroughProp={false}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <InputControl
                        placeHolder="Suite / Apt number"
                        onTextChange={this.changeVenueAddress}
                        value={venueAddress}
                        label="VENUE Address"
                      />
                    </div>
                    <div className="col-md-6">
                      <div className="input-control">
                        {/* <div class="input-control-label">CITY</div> */}
                        {/* <AutocompleteStyled
                          id="city"
                          onPlaceSelected={this.onPlaceSelected}
                          types={["(cities)"]}
                          componentRestrictions={{ country: "us" }}
                          placeholder="Enter city"
                        /> */}
                        <InputControl
                          placeHolder="Enter City Name"
                          onTextChange={this.changeCityInput}
                          value={city}
                          label="City"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <InputControl
                        placeHolder="Enter State Name"
                        onTextChange={this.changeStateInput}
                        value={venueState}
                        label="STATE"
                      />
                    </div>
                    <div className="col-md-6">
                      <InputControl
                        placeHolder="Enter Zip Code"
                        onTextChange={this.changeZipCode}
                        value={zipCode}
                        label="ZIP"
                      />
                    </div>
                  </div>

                  <MapContainer
                    onChange={this.onPositionChange}
                    draggable={true}
                    coordinates={this.state.coordinates}
                    // center={this.state.coordinates}
                  />
                  {/* <div className="row">
                    <div className="col-md-12"> 
                      <button className="btn-venue">Next</button>
                    </div>
                  </div> */}
                </div>
              </div>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <AddMedia
                mediaData={this.getMedia}
                featuresData={this.getFeatures}
                step={this.getStep}
              />
            </Form>
          </>
        ) : this.state.step === 2 ? (
          <AddAmenities
            saveVenueData={this.submitVenue}
            amenitiesData={this.getAmenities}
            specialFeaturesData={this.getSpecialFeatures}
            operatingHoursData={this.getOperatingHours}
            rulesData={this.getRules}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: isAuthenticated(state),
    categories: state.place.categories,
    capacities: state.place.capacities,
    areas: state.place.areas,
    saveVenue: state.place,
    allRates: state.allCurrencies.data
  };
};

export default connect(mapStateToProps, {
  getLocation,
  categoriesList,
  venueCapacities,
  venueAreas,
  createPlaces,
  getCurrentExchangeRate
})(BasicInfo);
