import React from 'react';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import NavMenu from '../../../components/Header/Menu';
import SearchForm from '../../../components/SearchForm/index';
import Account from '../../../components/Account/index';

import './style.scss';

const mapStateToProps = (state) => {
  return {};
};

class BookingPage extends React.PureComponent {
  render() {
    return (
      <div className="wrapper">
        <Container className="header booking">
          <NavMenu isAuthenticated={this.props.isAuthenticated}>
            <div className="header-content">
              <SearchForm />
              <Account />
            </div>
          </NavMenu>
        </Container>
      </div>
    );
  }
}

export default connect(mapStateToProps, {})(BookingPage);
