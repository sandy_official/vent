import React from "react";
import { connect } from "react-redux";
import { getPlaceDetails } from "../../../redux/actions/places.actions/places.thunk";
import { favoriteBuilding } from "../../../redux/actions/places.actions/places.thunk";
import { getFavoriteBuilding } from "../../../redux/actions/places.actions/places.thunk";
import { getReviews } from "../../../redux/actions/reviews.actions/reviews.thunk";
import { selectOperatingHours } from "../../../redux/selectors/places/places.select";
import { getMoreReviews } from "../../../redux/actions/reviews.actions/reviews.thunk";
import { askUserReview } from "../../../redux/actions/reviews.actions/reviews.thunk";
import { askHostReview } from "../../../redux/actions/reviews.actions/reviews.thunk";
import { getFavoriteState } from "../../../redux/selectors/places/places.select";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Spinner from "reactstrap/lib/Spinner";
import "./style.scss";

import { isAuthenticated } from "../../../redux/selectors/auth/auth.select";
import { getReviewsFromState } from "../../../redux/selectors/reviews";
import StartBooking from "../../../components/container/StartBooking";
import RulesPlace from "./components/RulesPlace";
import PhotoTiles from "./components/PhotoTiles";
import Header from "../../../components/Header";
import Footer from "../../../components/Footer";
import GeneralIntro from "./components/GeneralIntro";
import Features from "./components/Features";
import PlaceDetails from "./components/PlaceDetails";
import { specialFeatures, amenities } from "./constrains";
import GreatcityPlace from "./components/GreatcityPlace";

const mapStateToProps = state => {
    return {
        place: state.place.placeDetails,
        isAuthenticated: isAuthenticated(state),
        reviews: getReviewsFromState(state),
        isFavorite: getFavoriteState(state),
        operatingHours: selectOperatingHours(state)
    };
};

const LIMIT_REVIEWS = 5;

class DetailsPage extends React.PureComponent {
    state = {
        details: null,
        isOpen: false
    };

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.getPlaceDetails(id);
        this.props.getReviews(id, 0, LIMIT_REVIEWS);
        this.props.getFavoriteBuilding(id);
    }

    componentDidUpdate(prevProps) {
        const { id } = this.props.match.params;
        if (id !== prevProps.match.params.id) {
            this.props.getPlaceDetails(id);
            this.props.getReviews(id, 0, LIMIT_REVIEWS);
            this.props.getFavoriteBuilding(id);
        }
    }

    render() {
        const {
            isAuthenticated,
            place,
            reviews,
            favoriteBuilding,
            isFavorite,
            operatingHours,
            match
        } = this.props;
        const { id } = match.params;
        return (
            <div className="wrapper DetailsPage">
                <div className="header-section">
                    <Header isAuthenticated={isAuthenticated} find={true} />
                </div>
                <main className="container">
                    {place ? (
                        <Row className="row-container">
                            <Col className="pl-0" sm="8" md="8">
                                <PhotoTiles images={place.images} />
                                {place && (
                                    <GeneralIntro
                                        title={place.name}
                                        isAuth={isAuthenticated}
                                        isFavorite={isFavorite}
                                        favoriteBuilding={favoriteBuilding}
                                        rating={place.rating}
                                        address={place.address}
                                        city={place.city}
                                        reviewsCount={(reviews && reviews.count) || 0}
                                    />
                                )}
                                <Features
                                    title="Amenities"
                                    cardData={amenities}
                                    showRightMenu={true}
                                />
                                <Features title="Special Features" cardData={specialFeatures} />
                                <PlaceDetails content={place.aboutspace} />
                                <RulesPlace rules={place.rules} />
                            </Col>
                            <Col className="pl-2 pr-0" sm="4" md="4">
                                <StartBooking
                                    placeId={id}
                                    operatingHours={operatingHours}
                                    price={place.price}
                                />
                                <GreatcityPlace />
                            </Col>
                        </Row>
                    ) : (
                            <Spinner />
                        )}
                </main>
                <Footer />
            </div>
        );
    }
}

export default connect(mapStateToProps, {
    getPlaceDetails,
    getReviews,
    getMoreReviews,
    favoriteBuilding,
    getFavoriteBuilding,
    askUserReview,
    askHostReview
})(DetailsPage);
