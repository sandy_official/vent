import React, { Component } from "react";
import PropTypes from "prop-types";
import StarRatings from "react-star-ratings";
import { ratingColor } from "../../../constrains";
import user from "../../../../../../assets/testimonials/testimonial-2.png";
import "./style.scss";

class PlaceCard extends Component {
  render() {
    return (
      <div className="greatcity-place-card">
        <div className="greatcity-place-card-left">
          <img
            className="greatcity-place-card-left-img"
            src={user}
            alt="card-image"
          />
          <div>
            <StarRatings
              rating={4}
              starRatedColor={ratingColor}
              changeRating={this.changeRating}
              numberOfStars={5}
              starDimension="10px"
              starSpacing="2px"
              name="rating"
            />
          </div>
        </div>
        <div className="greatcity-place-card-right">
          <div className="greatcity-place-card-right-header">
            <div className="greatcity-place-card-right-header-title">
              Dr. Rose Brown
            </div>
            <div className="greatcity-place-card-right-header-more">
              13 Mar 2020
            </div>
          </div>
          <div className="greatcity-place-card-right-content">
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of using Lorem Ipsum
          </div>
        </div>
      </div>
    );
  }
}

PlaceCard.propType = {
  id: PropTypes.string
};

export default PlaceCard;
