import React, { Component } from "react";
import PropTypes from "prop-types";
import "./style.scss";
import PlaceCard from "./PlaceCard";

class GreatcityPlace extends Component {
  render() {
    return (
      <div className="greatcity-place">
        <div className="header">
          <div className="heading-2">
            Greatcity Place <span className="number">(105)</span>
          </div>
          <div className="header-more">View More</div>
        </div>
        <div className="content">
          <PlaceCard />
          <PlaceCard />
          <PlaceCard />
          <PlaceCard />
        </div>
      </div>
    );
  }
}

GreatcityPlace.propType = {
  id: PropTypes.string
};

export default GreatcityPlace;
