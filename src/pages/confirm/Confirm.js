import React from 'react';
import { Container } from 'reactstrap';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

import { connect } from 'react-redux';

class Confirm extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <Container>
          <Header isAuthenticated={this.props.isAuthenticated} find={true} />
          PAYMENTS
          <Footer />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated
  };
};

export default connect(mapStateToProps, {})(Confirm);
