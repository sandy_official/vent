import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import onClickOutside from 'react-onclickoutside';

import { logOut } from '../../../redux/actions/users.actions/auth.actions';
import history from '../../../history';
import Avatar from '../../Avatar';
import arrowDown from '../../../assets/icons/arrow_down.png';

import './style.scss';

function DropdownProfile(props) {
  const [isOpen, setIsOpen] = useState(false);

  const onLogout = () => {
    props.logOut();
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('id');

    history.push('/home');
  };

  const toggleModal = () => {
    setIsOpen(!isOpen);
  };

  DropdownProfile.handleClickOutside = () => setIsOpen(false);

  return (
    <div className="DropdownProfile">
      <div className="DropdownProfile__user" onClick={toggleModal}>
        <Avatar account={true} height={50} width={50} />
        <img src={arrowDown} alt="arrow" className="DropdownProfile__arrow" />
      </div>
      {isOpen && (
        <ul className="DropdownProfile__menu">
          <Link to="/profile">
            <li className="DropdownProfile__item">My profile</li>
          </Link>
          <Link to="#">
            <li className="DropdownProfile__item">Reviews</li>
          </Link>
          <Link to="/buildings">
            <li className="DropdownProfile__item">Add Venue</li>
          </Link>
          <li className="DropdownProfile__item" onClick={onLogout}>
            Log Out
          </li>
        </ul>
      )}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {};
};

const clickOutsideConfig = {
  handleClickOutside: () => DropdownProfile.handleClickOutside
};

export default connect(mapStateToProps, { logOut })(
  onClickOutside(DropdownProfile, clickOutsideConfig)
);
