import React from "react";
import PropTypes from "prop-types";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Helmet from "react-helmet";
import { DateRangePicker } from "react-dates";
import { START_DATE, END_DATE } from "react-dates/constants";
import moment from "moment";
import { Label } from "reactstrap";
import styled from "styled-components";
import "./style.scss";

const LabelStyled = styled(Label)`
  width: 50% !important;
  font-size: 13px !important;
  font-weight: 400 !important;
  padding-left: 11px !important;
  margin-bottom: 0px !important;
`;

export class AirbnbRangeDatesPicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      focusedInput: null,
      startDate: props.startDate || moment(),
      endDate: props.endDate || moment().add(1, "days")
    };
  }

  onDatesChange = ({ startDate, endDate }) => {
    let { focusedInput } = this.state;

    if (focusedInput === START_DATE) {
      endDate = startDate;
    }

    if (focusedInput === END_DATE && endDate === null) {
      endDate = startDate;
    }

    this.setState({ startDate, endDate, focusedInput });

    this.props.liftUpDates(startDate, endDate);
  };

  render() {
    const { focusedInput, startDate, endDate } = this.state;

    return (
      <>
        <div className="label-wrapper">
          <LabelStyled>Start date</LabelStyled>
          <LabelStyled>End Date</LabelStyled>
        </div>
        <DateRangePicker
          startDate={startDate} // momentPropTypes.momentObj or null,
          startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
          endDate={endDate} // momentPropTypes.momentObj or null,
          endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
          onDatesChange={this.onDatesChange} // PropTypes.func.isRequired,
          focusedInput={focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
          onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired
          noBorder
          customArrowIcon={null}
        />
        <Helmet>
          <style>{`
          .DateRangePicker div .DateRangePickerInput {
            width: 100%;
            margin-top: 1px;
          }
          .DateRangePicker div .DateRangePickerInput .DateInput {
            width: 47%;
          }
          `}</style>
        </Helmet>
      </>
    );
  }
}

AirbnbRangeDatesPicker.propTypes = {
  liftUpDates: PropTypes.func,
  startDate: PropTypes.object, // moment object
  endDate: PropTypes.object // moment object
};
