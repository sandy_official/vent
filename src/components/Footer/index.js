import React, { useState } from "react";
import Logo from "../Header/FullLogo";
import { Link } from "react-router-dom";
import moment from "moment";
import "./styles.scss";
import PreFooter from "./PreFooter";
import Facebook from "../../assets/icons/Facebook.svg";
import Instagram from "../../assets/icons/Instagram.svg";

export default () => {
  const [input, setInput] = useState("");
  const [showInvalidEmail, setInvalidEmail] = useState(false);
  const validateEmail = mail => {
    const regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(String(mail).toLowerCase());
  };
  const subscribeUser = () => {
    if (validateEmail(input)) {
      console.log("Subscribe user here!");
      setInvalidEmail(false);
    } else {
      setInvalidEmail(true);
    }
  };
  return (
    <>
      <PreFooter />
      <footer className="footer">
        <div className="container footer__wrapper">
          <div className="footer__large-box">
            <h2 className="footer__title">About us</h2>
            <div className="footer__black"></div>
            <p className="footer__text">
              Ontrary to popular belief, Lorem Ipsum is not simply random text.
              It has roots in a piece of classical Latin literature from 45 BC,
              making it over 2000 years oldOntrary to popular belief, Lorem
              Ipsum is not simply random text. It has roots in a piece of
              classical Latin literature from .
            </p>
          </div>

          <div className="footer__block">
            <h2 className="footer__title">Heading Text</h2>
            <div className="footer__black"></div>
            <ul className="footer__nav">
              <li>
                <a className="footer__link" href="#">
                  Ontrary
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Popular belief
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Lorem ipsum
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Simply
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Random
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__block">
            <h2 className="footer__title">Heading Text</h2>
            <div className="footer__black"></div>
            <ul className="footer__nav">
              <li>
                <a className="footer__link" href="#">
                  Ontrary
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Popular belief
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Lorem ipsum
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Simply
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Random
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__block">
            <h2 className="footer__title">Heading Text</h2>
            <div className="footer__black"></div>
            <ul className="footer__nav">
              <li>
                <a className="footer__link" href="#">
                  Ontrary
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Popular belief
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Lorem ipsum
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Simply
                </a>
              </li>
              <li>
                <a className="footer__link" href="#">
                  Random
                </a>
              </li>
            </ul>
          </div>

          <div className="footer__connect">
            <h2 className="footer__heading">
              So what are you waiting for ? Contact for a Quick Quote
            </h2>
            <h2 className="footer__numbers">631.000.3737</h2>
            <h2 className="footer__subheading">SUBSCRIBE FOR UPDATES</h2>
            <div className="footer__input--wrapper">
              <input
                className="footer__input"
                type="text"
                placeholder="Enter your email"
              />
              <button className="footer__button" type="button">
                Submit
              </button>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};
