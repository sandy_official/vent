import React from "react";

const PreFooter = () => {
  return (
    <div className="prefooter">
      <div className="container prefooter__wrapper">
        <div className="prefooter__left">
          <div className="prefooter__block">
            <img src="/images/icons/phone-icon.png" alt="phone icon" />
            <span className="prefooter__text">631.000.3737</span>
          </div>
          <div className="prefooter__block">
            <img src="/images/icons/email-icon.png" alt="email icon" />
            <span className="prefooter__text">vent@example.com</span>
          </div>
          <div className="prefooter__block">
            <img src="/images/icons/location-icon.png" alt="location icon" />
            <span className="prefooter__text">
              734 Walt Whitman Rd. Suite 203, Melville, NY 11747
            </span>
          </div>
        </div>

        <div className="prefooter__right">
          <img
            className="prefooter__icon"
            src="/images/icons/skype.svg"
            alt="skype icon"
          />
          <img
            className="prefooter__icon"
            src="/images/icons/f.svg"
            alt="facebook icon"
          />
          <img
            className="prefooter__icon"
            src="/images/icons/g.svg"
            alt="google icon"
          />
          <img
            className="prefooter__icon"
            src="/images/icons/twit.svg"
            alt="twitter icon"
          />
          <img
            className="prefooter__icon"
            src="/images/icons/linked.svg"
            alt="linkedin icon"
          />
          <img
            className="prefooter__icon"
            src="/images/icons/you.svg"
            alt="youtube icon"
          />
        </div>
      </div>
    </div>
  );
};

export default PreFooter;
