import React from "react";
import PropType from "prop-types";
import starPng from "./assets/star.png";
import Slider from "react-slick";
import ReactPlayer from "react-player";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./style.scss";
import InfoIcon from "../../assets/icons/info-icon.png";
import HeartIcon from "../../assets/icons/heart-icon.png";

export default function PlaceCard({ place, order }) {
  const SlickButtonFix = ({ currentSlide, slideCount, ...props }) => (
    <span {...props}></span>
  );

  const settings = {
    dots: false,
    infinite: true,
    speed: 700,
    prevArrow: <SlickButtonFix />,
    nextArrow: <SlickButtonFix />,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  // const styles = {
  //   marginLeft: order === 0 ? '0' : '15px',
  //   marginRight: order === 4 ? '0': '15px',
  // };

  return (
    <div className="place-card">
      <div className="place-card-image">
        {/* <Slider {...settings}>
          {place.image
            .filter((i) => i.type === 1)
            .map((item, i) => {
              return item.type === 1 ? (
                <img key={i} src={item.link} alt="img" className="place-card-image-img" />
              ) : (
                // TODO: fix preloading videos because a lot of videos are preloaded and consume a lot of memory
                <ReactPlayer
                  key={i}
                  className="video-item-card"
                  width="300px"
                  height="233px"
                  controls={true}
                  url={item.link}
                  fileConfig={{
                    autoplay: false
                  }}
                />
              );
            })}
        </Slider> */}
        <img
          src={place.image}
          alt={place.name}
          className="place-card-image-img"
        />
      </div>
      <div className="place-card-title-wrapper">
        <span className="place-card-title">{place.name}</span>
        <img src={InfoIcon} alt="info icon" />
      </div>
      <p className="place-card-text">{place.text}</p>
      <div className="place-card-price-wrapper">
        <span className="place-card-price">${place.price}</span>/
        <span className="place-card-persons">{place.per}</span>
      </div>

      <div className="place-card-capacity">
        <span className="place-card-capacity-amount">
          {place.persons} Persons
        </span>
        |
        <div className="place-card-rate-wrapper">
          <span className="place-card-rate">{place.rate}</span>
          <img src={HeartIcon} alt="heart icon" />
        </div>
      </div>
    </div>
  );
}

PlaceCard.propType = {
  place: PropType.object
};
