import React from "react";
import TimeKeeper from "react-timekeeper";
import PropTypes from "prop-types";
import "./style.scss";
import moment from "moment";
import arrowDownPng from "./assets/check-box.png";
import onClickOutside from "react-onclickoutside";
import styled from "styled-components";
import { Label } from "reactstrap";

const LabelStyled = styled(Label)`
  font-family: Inter;
  font-size: 12px;
  font-weight: 700;
  text-transform: uppercase;
`;

class TimePicker extends React.Component {
  handleClickOutside = () => {
    if (!this.props.multiPicker) {
      this.props.setShowTime(false);
    }
  };

  handleDoneClick = newTime => {
    this.props.setTime(newTime.formatted24);
    this.props.setShowTime(false);
  };

  render() {
    const { time, showTime, setShowTime, text, id } = this.props;

    return (
      <div className="timekeeper">
        <LabelStyled>{text}</LabelStyled>
        {showTime && (
          <div className="timekeeper-modal">
            <TimeKeeper
              time={time}
              onDoneClick={newTime => this.handleDoneClick(newTime)}
              switchToMinuteOnHourSelect
            />
          </div>
        )}
        {!showTime && (
          <div
            className={`timekeeper-btn timekeeper-btn--${id}`}
            onClick={() => setShowTime(true)}
          >
            {moment(time, "H:mm").format("hh:mm a")}
            <img src={arrowDownPng} alt="arrow-down" />
          </div>
        )}
      </div>
    );
  }
}

export default onClickOutside(TimePicker);

TimePicker.propType = {
  time: PropTypes.string,
  setTime: PropTypes.func
};
