import React from "react";
// import Dropdown from "../../Dropdown";
import CustomDropdown from "../../Dropdown/CustomDropdown";
import "./style.scss";

const MultiselectInputAddOn = prop => {
  const {
    label,
    placeHolder,
    value,
    onTextChange,
    onDropDownChange,
    onCurrencyDropDownChange,
    item,
    listOfItem,
    RateItem,
    listOfAllRates,
    openStatus,
    enableThroughPropCurrency,
    enableThroughPropChargeType,
    currencyOnClickHandleOutside,
    currencyOnClickHandle,
    chargeTypeOnClickHandleOutside,
    chargeTypeOnClickHandle,
    currencyStatus,
    chargeTypeStatus
  } = prop;
  const onInputChange = value => {
    onTextChange(value);
  };

  const dropDownChange = value => {
    // console.log("DropDown Values -->", value);
    onDropDownChange(value);
  };

  const dropDownChangeCurrency = currency => {
    onCurrencyDropDownChange(currency);
  };

  // const openStatus = status => {
  //   console.log("Status-->", status);
  // };
  return (
    <div className="input-control">
      <div className="input-control-label">{label}</div>
      <div className="addOnDdlWrapper">
        <input
          className="inputfield"
          onChange={onInputChange}
          value={value}
          placeholder={placeHolder}
        ></input>
        <CustomDropdown
          //   className="addOnDdl"
          changeItem={dropDownChangeCurrency}
          item={RateItem}
          list={listOfAllRates}
          customRef="currency"
          openStatus={openStatus}
          enableThroughProp={enableThroughPropCurrency}
          onClickHandleOutside={currencyOnClickHandleOutside}
          onClickHandle={currencyOnClickHandle}
          status={currencyStatus}
        ></CustomDropdown>
        <CustomDropdown
          //   className="addOnDdl"
          changeItem={dropDownChange}
          item={item}
          list={listOfItem}
          customRef="chargeType"
          openStatus={openStatus}
          enableThroughProp={enableThroughPropChargeType}
          onClickHandleOutside={chargeTypeOnClickHandleOutside}
          onClickHandle={chargeTypeOnClickHandle}
          status={chargeTypeStatus}
        ></CustomDropdown>
      </div>
    </div>
  );
};
export default MultiselectInputAddOn;
