import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NavMenu from './NavMenu';
import Menu from './Menu';

export default class Header extends Component {
  render() {
    const { isAuthenticated, find, isAdmin } = this.props;
    return (
      <NavMenu find={find} isAdmin={isAdmin}>
        <Menu isAuthenticated={isAuthenticated} isAdmin={isAdmin} />
      </NavMenu>
    );
  }
}

Header.propTypes = {
  isAuthenticated: PropTypes.bool,
  find: PropTypes.bool
};
