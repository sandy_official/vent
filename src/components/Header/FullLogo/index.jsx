import React from "react";
import fullLogo from "../../../assets/full-logo.png";
import fullLogoBlack from "../../../assets/Full_Logo.png"
import "./style.scss";

const FullLogo = () => {
  return <img className="fullLogo" src={fullLogo} alt="Full Logo" />;
};

export const FullLogoBlack = () =>{
  return <img className="fullLogoBlack" src={fullLogoBlack} alt="Full Logo" />;
}

export default FullLogo;
