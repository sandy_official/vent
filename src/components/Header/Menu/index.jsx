import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import DropdownProfile from "../../styledComp/DropDownProfile";
import SignInModal from "../../SignInBtn/SignInBtn";
import "./style.scss";

class Menu extends Component {
  render() {
    const {
      location: { pathname }
    } = this.props;
    const { isAuthenticated, isAdmin } = this.props;
    return (
      <div className="Menu-header-links">
        {/* <Link className="Menu-header-item" to="/">
          Host
        </Link>
        <Link className="Menu-header-item" to="/Guest">
          Guest
        </Link> */}
        <Link className="Menu-header-item" to="/faq">
          FAQ
        </Link>
        <Link className="Menu-header-item" to="/contact">
          CONTACT
        </Link>
        {isAdmin ? (
          <Link
            className="Menu-header-item signin-hover"
            to={pathname === "/admin" ? "/admin/signup" : "/admin"}
          >
            {pathname === "/admin" ? "Signup" : "Signin"}
          </Link>
        ) : isAuthenticated ? (
          <DropdownProfile />
        ) : (
          <SignInModal />
        )}
      </div>
    );
  }
}

export default withRouter(Menu);
