import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment";
import { saveBookingDataLocally } from "../../../redux/actions/booking.actions/booking.thunk";
import history from "../../../history";
import TimePiker from "../../TimePicker";
import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";
import Button from "reactstrap/lib/Button";

import { SelectInputControl } from "../../../components/InputControls";
// import CardTitle from 'reactstrap/lib/CardTitle';
// import CardBody from 'reactstrap/lib/CardBody';
// import Card from 'reactstrap/lib/Card';
import { AirbnbRangeDatesPicker } from "../../styledComp/AirbnbRangeDatesPicker";
import { setBookingParameters } from "../../../redux/actions/booking.actions/booking.actions";
// import arrowDownPng from './assets/check-box.png';
// import arrowUpPng from './assets/check-box-up.png';
import "./style.scss";

const mapStateToProps = state => {
  return { booking: state.booking };
};

class StartBooking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: moment(),
      endDate: moment().add(1, "days"),
      startTime: "00:00",
      endTime: "23:59",
      guest: { count: "1-30", price: 0 },
      showSTime: false,
      showETime: false,
      isOpenDropdown: false,
      dateError: false
    };
  }

  liftUpDates = (startDate, endDate) => {
    this.setState({
      startDate,
      endDate
    });
  };

  changeSTime(newTime) {
    this.setState({ startTime: newTime });
  }

  changeETime(newTime) {
    this.setState({ endTime: newTime });
  }

  changeShowSTime(value) {
    if (!this.state.showETime) {
      this.setState({ showSTime: value });
    }
  }
  changeShowETime(value) {
    if (!this.state.showSTime) {
      this.setState({ showETime: value });
    }
  }

  changeGuest(newValue) {
    this.setState({ guest: newValue, isOpenDropdown: false });
  }

  toggleDropdown() {
    this.setState(prevState => ({ isOpenDropdown: !prevState.isOpenDropdown }));
  }

  checkOperationHours() {
    const startDateSec = this.state.startDate / 1000;
    const endDateSec = this.state.endDate / 1000;

    const startDateTime = moment.unix(startDateSec);
    const endDateTime = moment.unix(endDateSec);

    const { hour: startHour, minute: startMinute } = moment(
      this.state.startTime,
      "H:mm"
    );

    const { hour: endHour, minute: endMinute } = moment(
      this.state.endTime,
      "H:mm"
    );

    const startDate = startDateTime
      .hour(startHour)
      .minute(startMinute)
      .toISOString();

    const endDate = endDateTime
      .hour(endHour)
      .minute(endMinute)
      .toISOString();

    this.props.saveBookingDataLocally({
      startDate,
      endDate,
      placeId: this.props.placeId,
      guest: this.state.guest
    });

    const { setBookingParameters } = this.props;
    setBookingParameters(this.state);
    history.push("/request-booking");
  }

  render() {
    const { price } = this.props;
    const {
      startTime,
      endTime,
      guest,
      showSTime,
      showETime,
      isOpenDropdown,
      dateError
    } = this.state;

    return (
      <div className="startBooking">
        <div className="startBooking-title">
          <span className="startBooking-title-price">
            $ {price} <span className="startBooking-title-hour">/ Hour</span>
          </span>
        </div>
        <div className="startBooking-content">
          <AirbnbRangeDatesPicker
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            liftUpDates={this.liftUpDates}
          />
          <Row className="startBooking-content-p10">
            <Col>
              <TimePiker
                text="Start Time"
                time={startTime}
                setTime={value => this.changeSTime(value)}
                showTime={showSTime}
                setShowTime={value => this.changeShowSTime(value)}
              />
            </Col>
            <Col>
              <TimePiker
                text="End Time"
                time={endTime}
                setTime={value => this.changeETime(value)}
                showTime={showETime}
                setShowTime={value => this.changeShowETime(value)}
              />
            </Col>
            {dateError && (
              <div className="startBooking-content-error">
                <span className="startBooking-content-error-text">
                  Please choice another Date and Time
                </span>
              </div>
            )}
          </Row>
          <Row>
            <Col className="startBooking-content-p10" sm="12" md="12">
              <SelectInputControl
                label="PEOPLE RANGE"
                selectOptions={["10-20", "20-30", "30-50", "50-100", "100-500"]}
                onDropDownChange={() => {}}
                placeHolder="10-20"
              />
            </Col>
            <Col className="startBooking-content-booking" sm="12" md="12">
              <button
                className="startBooking-content-booking-btn"
                onClick={() => this.checkOperationHours()}
              >
                Request to book
              </button>
              <div className="startBooking-content-booking-info">
                <span>Cancel for free within 24 hours</span>
              </div>
            </Col>
          </Row>
          <div className="startBooking-content-review">
            <button className="startBooking-content-review-btn">
              Ask User Review
            </button>
            <button className="startBooking-content-review-btn">
              Ask Host Review
            </button>
          </div>
        </div>
      </div>
    );
  }
}

StartBooking.propTypes = {
  placeId: PropTypes.string,
  operatingHours: PropTypes.array,
  price: PropTypes.string
};

export default connect(mapStateToProps, {
  saveBookingDataLocally,
  setBookingParameters
})(StartBooking);
