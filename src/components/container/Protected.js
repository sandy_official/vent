import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { checkAuth } from '../../redux/actions/users.actions/auth.thunk';
import ProfilePage from '../../pages/user/profilePage';
import Admin from '../../pages/user/admin';
import Host from '../../pages/place/hostPage';
import Buildings from '../../pages/place/createPlace';
import EditPlace from '../../pages/place/editPlace';
import UploadFiles from '../../pages/place/uploadFiles';
import Container from 'reactstrap/lib/Container';
import Header from '../Header';

class Protected extends React.Component {
  componentDidMount() {
    this.props.checkAuth();
  }
  render() {
    //if (!this.props.isAuthenticated && !this.props.checkAuthError) return null;
    //if (!this.props.isAuthenticated && this.props.checkAuthError) return <Redirect to="/home" />;
    //if (this.props.isAuthenticated && !this.props.loginError)
    return (
      <>
      <div className="top_header">
          <Header isAuthenticated={this.props.isAuthenticated} />
        </div>
      <div className="section">
        <Switch>
          <Route exact path="/profile" component={ProfilePage} />
          <Route exact path="/admin" component={Admin} />
          <Route exact path="/host" component={Host} />
          <Route exact path="/edit-place/:id" component={EditPlace} />
          <Route exact path="/buildings" component={Buildings} />
          <Route exact path="/upload-files/:id" component={UploadFiles} />
        </Switch>
      </div>
      </>
    );
  }
}

export default connect(
  (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    checkAuthError: state.auth.checkAuthError,
    loginError: state.auth.loginError
  }),
  {
    checkAuth
  }
)(Protected);
