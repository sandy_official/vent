import React, { Component } from 'react';
import Event from './Event';
import Location from './Location';
import Dates from './Dates';
import Attendees from './Attendees';
import Price from './Price';
import SeparateLine from '../../components/SeparateLine';
import Container from 'reactstrap/lib/Container';

import './style.scss';

class Filter extends Component {
  render() {
    const {
      event,
      location,
      startDate,
      endDate,
      guests,
      price,
      onChangeFilters,
      onPlaceSelected,
      changeLocationState
    } = this.props;

    return (
      <Container className="filter">
        <div className="filter-row">
          <Event changeState={onChangeFilters} event={event} />
          <Location
            changeState={changeLocationState}
            onPlaceSelected={onPlaceSelected}
            location={location}
          />
          <Dates changeState={onChangeFilters} startDate={startDate} endDate={endDate} />
          <Attendees changeState={onChangeFilters} guests={guests} />
          <Price changeState={onChangeFilters} price={price} />
        </div>
        <SeparateLine />
      </Container>
    );
  }
}

export default Filter;
