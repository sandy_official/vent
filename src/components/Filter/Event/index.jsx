import React, { useState } from 'react';
import Input from '../Input';
import onClickOutside from 'react-onclickoutside';

import './style.scss';

const data = ['Wedding Venues', 'Conference Room', 'Rooftop Terrace'];

function Event({ event, changeState }) {
  const [show, setShow] = useState(false);

  const change = (value) => {
    changeState(value, 'event');
    setShow(false);
  };

  Event.handleClickOutside = () => {
    setShow(false);
  };

  return (
    <div className="event" onClick={() => setShow(!show)}>
      <Input>
        <span className="event-text">{event}</span>
      </Input>

      {show && (
        <ul className="event-menu">
          {data &&
            data.map((item, i) => (
              <li className="event-menu-li" onClick={() => change(item)} key={`event_${i}`}>
                <span>{item}</span>
              </li>
            ))}
        </ul>
      )}
    </div>
  );
}

const clickOutsideConfig = {
  handleClickOutside: () => Event.handleClickOutside
};

export default onClickOutside(Event, clickOutsideConfig);
