import React from 'react';
import './style.scss';
import LocationIcon from './assets/location.png';
import Autocomplete from 'react-google-autocomplete';

export default function Location({ changeState, location, onPlaceSelected }) {
  return (
    <div className="filter-location">
      <img className="filter-location-icon" src={LocationIcon} alt="loc" />
      <Autocomplete
        className="filter-location-input"
        type="text"
        onPlaceSelected={(place) => onPlaceSelected(place)}
        onChange={(e) => changeState(e.target.value, 'location')}
        value={location}
        placeholder="Location"
        componentRestrictions={{ country: 'us' }}
        language="en"
      />
    </div>
  );
}
