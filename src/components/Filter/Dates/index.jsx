import React, { Component } from 'react';
import DateTimePiker from '../../DateTimePiker';
import Input from '../Input';
import './style.scss';
import Calendar from './assets/calendar.png';
import onClickOutside from 'react-onclickoutside';
import moment from 'moment';

class Dates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: this.props.startDate,
      endDate: this.props.endDate,
      showModal: false,
      validDateRange: true
    };
  }

  handleClickOutside = (evt) => {
    if (this.state.validDateRange) {
      this.setState({ showModal: false });
    } else {
      evt.stopPropagation();
    }
  };

  toggleShowModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  disableClkOutside = (bool) => {
    this.setState({ validDateRange: bool });
  };

  setDateTimeToParams(value) {
    if (value) {
      this.props.changeState(value.startDate, 'startDate');
      this.props.changeState(value.endDate, 'endDate');
      this.setState({ startDate: value.startDate, endDate: value.endDate });
    }
    this.toggleShowModal();
  }

  setInputValue = () => {
    const { startDate, endDate } = this.state;
    const firstTime = moment(startDate).format('MMM D, h:mm a - ');
    const endTime = moment(endDate).format('h:mm a');
    if (firstTime !== 'Invalid date' || endTime !== 'Invalid date') {
      return `${firstTime}${endTime}`;
    }
    return 'When?';
  };

  render() {
    const { showModal } = this.state;
    const { startDate, endDate } = this.props;

    return (
      <div className="filter-dates">
        <Input>
          <div className="filter-dates-block" onClick={() => this.toggleShowModal()}>
            <img className="filter-dates-block-img" src={Calendar} alt="" />
            <span className="filter-dates-block-input">
              {startDate && endDate ? this.setInputValue() : 'When?'}
            </span>
          </div>
        </Input>
        {showModal && (
          <DateTimePiker
            setDateTime={(value) => this.setDateTimeToParams(value)}
            startDate={startDate}
            endDate={endDate}
            checkValid={this.disableClkOutside}
          />
        )}
      </div>
    );
  }
}

export default onClickOutside(Dates);
