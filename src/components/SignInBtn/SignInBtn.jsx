import React, { useState } from "react";
import signIn from "../../assets/icons/sign_in.png";
import "./style.scss";
import SignInModal from "../SignInModal";
import { Link } from "react-router-dom";

const SignInBtn = () => {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Link to="/signIn" className="signIn-btn signin-hover" onClick={toggle}>
        SIGN IN
      </Link>
      {/* <SignInModal toggle={toggle} modal={modal} /> */}
      {/* <img
        className="drop-down"
        src="/images/icons/drop-down-white.png"
        alt="drop down"
      /> */}
    </div>
  );
};

export default SignInBtn;
