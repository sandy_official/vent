import React, { Suspense } from "react";
import { Router, Route, Switch } from "react-router-dom";
import history from "./history";
import SignUp from "./pages/user/signUp";
import SignIn from "./pages/user/signIn";
import HomePage from "./pages/home";
import Protected from "./components/container/Protected";
import ActivateCode from "./pages/user/activateCode";
import ListingPage from "./pages/listing/ListingPage";
import ResetPassword from "./pages/user/resetPassword";
import updatePassword from "./pages/user/updatePassword";
import CheckEmail from "./pages/user/resetPassword/checkEmail";
import PlacesDetails from "./pages/place/detailsPage";
import BookingPage from "./pages/place/bookingPage";
import requestBookPage from "./pages/place/requestBookPage";
import UserRatePage from "./pages/place/userRatePage";
import HostRatePage from "./pages/place/hostRatePage";
import Confirm from "./pages/confirm/Confirm";
import Contact from "./pages/contact/";
import FAQ from "./pages/faq";
import Admin from "./pages/adminDashboard/index";
import EmailSent from "./pages/user/admin/signup/EmailSent";
import NotFound from "./components/NotFound/NotFound";

function App() {
  return (
    <div className="ui">
      <Router history={history}>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/signUp" component={SignUp} />
            <Route exact path="/signIn" component={SignIn} />
            <Route exact path="/home" component={HomePage} />
            <Route exact path="/activate" component={ActivateCode} />
            <Route exact path="/listing" component={ListingPage} />
            <Route
              exact
              path="/reset-password/:token"
              component={ResetPassword}
            />
            <Route exact path="/update-password" component={updatePassword} />
            <Route exact path="/check-email" component={CheckEmail} />
            <Route exact path="/place-details/:id" component={PlacesDetails} />
            <Route exact path="/booking/:id" component={BookingPage} />
            <Route exact path="/request-booking" component={requestBookPage} />
            <Route exact path="/user-rate/:token" component={UserRatePage} />
            <Route exact path="/host-rate/:token" component={HostRatePage} />
            <Route exact path="/payment" component={Confirm} />
            <Route exact path="/email-sent" component={EmailSent} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/faq" component={FAQ} />
            <Route path="/admin" component={Admin} />
            <Route path="/" component={Protected} />
            <Route component={NotFound} />
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;
