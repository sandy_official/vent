import produce from 'immer';
import {
  START_BOOKING_REQUEST,
  START_BOOKING_FAILURE,
  START_BOOKING_SUCCESS,
  SET_BOOKING_PARAMETERS,
  SET_BOOKING_ACTIVITY,
  SET_BOOKING_GUESTS,
  SET_BOOKING_DATE_TIME,
} from '../../actionsBookingTypes';

const initialState = JSON.parse(sessionStorage.getItem('booking')) || {};

export default produce((draft = initialState, action) => {

  switch (action.type) {
    case START_BOOKING_REQUEST:
      return draft;

    case START_BOOKING_SUCCESS:
      return [...draft, action.payload];

    case START_BOOKING_FAILURE:
      return draft;
    case SET_BOOKING_PARAMETERS: {
      const {
        startDate, endDate, startTime, endTime, guest
      } = action.payload;
      return {
        ...draft,
        parameters: {
          startDate: startDate,
          endDate: endDate,
          startTime: startTime,
          endTime: endTime,
          guest: guest
        }
      };
    }
    case SET_BOOKING_ACTIVITY: return {
      ...draft,
      parameters: {
        ...draft.parameters,
        activity: action.payload,
      }

    };
    case SET_BOOKING_GUESTS: return {
      ...draft,
      parameters: {
        ...draft.parameters,
        guest: {
          count: action.payload,
          price: 0
        },
      }

    };




    default:
      return draft;
  }
});
