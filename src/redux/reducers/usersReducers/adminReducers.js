import { ADMIN_REQUEST, ADMIN_SUCCESS, ADMIN_FAILURE } from '../../actionUserTypes';

const initialState = {
  loading: false,
  error: null,
  data: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADMIN_REQUEST: {
      return {
        ...state,
        loading: true
      };
    }
    case ADMIN_SUCCESS: {
      return {
        ...state,
        data: action.payload,
        loading: false
      };
    }
    case ADMIN_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    }
    default:
      return state;
  }
};
