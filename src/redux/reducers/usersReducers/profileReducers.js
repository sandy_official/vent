import {
  EDIT_REQUEST,
  EDIT_SUCCESS,
  EDIT_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
  CHECK_USER_EXISTENCE_REQUEST,
  CHECK_USER_EXISTENCE_SUCCESS,
  CHECK_USER_EXISTENCE_FAILURE,
  SEND_CONTACTS_SUCCESS,
  SEND_CONTACTS_FAILURE
} from '../../actionUserTypes';

const initialState = {
  editLoading: false,
  editError: '',
  deleteLoading: false,
  deleteError: '',
  user: {
    email: '',
    first_name: '',
    last_name: '',
    role: {
      role_type: ''
    },
    password: ''
  },
  isSendContacts: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHECK_USER_EXISTENCE_REQUEST: {
      return {
        ...state,
        editLoading: true
      };
    }
    case CHECK_USER_EXISTENCE_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        editLoading: false
      };
    }
    case CHECK_USER_EXISTENCE_FAILURE: {
      return {
        ...state,
        editLoading: false,
        editError: action.payload
      };
    }
    case EDIT_REQUEST: {
      return {
        ...state,
        editLoading: true
      };
    }
    case EDIT_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        editLoading: false
      };
    }
    case EDIT_FAILURE: {
      return {
        ...state,
        editLoading: false,
        editError: action.payload
      };
    }
    case DELETE_USER_REQUEST: {
      return {
        ...state,
        deleteLoading: true
      };
    }
    case DELETE_USER_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        deleteLoading: false
      };
    }
    case DELETE_USER_FAILURE: {
      return {
        ...state,
        deleteLoading: false,
        deleteError: action.payload
      };
    }
    case SEND_CONTACTS_SUCCESS: {
      return {
        ...state,
        isSendContacts: true
      };
    }
    case SEND_CONTACTS_FAILURE: {
      return {
        ...state,
        isSendContacts: false
      };
    }
    default:
      return state;
  }
};
