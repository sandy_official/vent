import {
  ADMIN_DASHBOARD_REQUEST,
  ADMIN_DASHBOARD_SUCCESS,
  ADMIN_DASHBOARD_FAILURE
} from '../../../redux/adminActionTypes';

const initialState = {
  dashboardLoading: false,
  dashboardError: null,
  dashboardData: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADMIN_DASHBOARD_REQUEST: {
      return {
        ...state,
        dashboardLoading: true
      };
    }
    case ADMIN_DASHBOARD_SUCCESS: {
      return {
        ...state,
        dashboardData: action.payload,
        dashboardLoading: false
      };
    }
    case ADMIN_DASHBOARD_FAILURE: {
      return {
        ...state,
        dashboardLoading: false,
        dashboardError: action.payload
      };
    }
    default:
      return state;
  }
};
