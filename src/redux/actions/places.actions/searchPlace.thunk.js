import * as actions from './searchPlace.actions';
// import history from '../../../history';
import axiosInstance from '../../../api';
import moment from 'moment';

export const getCities = () => async (dispatch) => {
  dispatch(actions.getCitiesRequest());
  try {
    const result = await axiosInstance.get('/api/cities');
    dispatch(actions.getCitiesSuccess(result.data));
  } catch (error) {
    dispatch(actions.getCitiesFailure({ message: error.message }));
  }
};

export const searchCity = (city) => async (dispatch) => {
  dispatch(actions.searchCityRequest());
  try {
    const result = await axiosInstance.post('/api/buildings/mainfilter', city);
    dispatch(actions.searchCitySuccess(result.data));
  } catch (error) {
    dispatch(actions.searchCityFailure({ message: error.message }));
  }
};

export const searchFilter = (data) => async (dispatch) => {
  dispatch(actions.searchFilterRequest());
  try {
    const result = await axiosInstance.post('/api/buildings/mainfilter', data);
    dispatch(actions.searchFilterSuccess(result.data));
  } catch (error) {
    dispatch(actions.searchFilterFailure({ message: error.message }));
  }
};

export const getSearchPlace = (data) => async (dispatch) => {
  // const startDate = moment().toISOString();
  // const endDate = moment().toISOString();
  // console.log('@@', data);
  const { endDate, startDate, guests, skip, limit, event, price, locationSelected } = data;
  const size = guests === '31-200' ? 'large' : 'small';
  dispatch(actions.searchPlaceRequest());
  try {
    const result = await axiosInstance.get('/api/buildings/search', {
      params: {
        size,
        skip,
        limit,
        event,
        endPrice: price[1],
        startPrice: price[0],
        startDate,
        endDate,
        city: locationSelected
      }
    });
    dispatch(actions.searchPlaceSuccess(result.data));
  } catch (error) {
    dispatch(actions.searchPlaceFailure({ message: error.message }));
  }
};
