import { startBookingRequest, startBookingSuccess, startBookingFailure } from './booking.actions';

export const saveBookingDataLocally = (data) => async (dispatch) => {
  dispatch(startBookingRequest());
  try {
    await sessionStorage.setItem('booking', JSON.stringify(data));
    dispatch(startBookingSuccess(data));
  } catch (error) {
    dispatch(startBookingFailure(error));
  }
};
