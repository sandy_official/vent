import {
  START_BOOKING_REQUEST,
  START_BOOKING_FAILURE,
  START_BOOKING_SUCCESS,
  SET_BOOKING_PARAMETERS,
  SET_BOOKING_ACTIVITY,
  SET_BOOKING_GUESTS,
  SET_BOOKING_DATE_TIME,
} from '../../actionsBookingTypes';

export const startBookingRequest = () => ({
  type: START_BOOKING_REQUEST
});

export const startBookingSuccess = (data) => ({
  type: START_BOOKING_SUCCESS,
  payload: data
});

export const startBookingFailure = (err) => ({
  type: START_BOOKING_FAILURE,
  payload: err
});

export const setBookingParameters = (data) => {
  return {
    type: SET_BOOKING_PARAMETERS,
    payload: data
  }
};

export const changeActivity = (data) => {
  return {
    type: SET_BOOKING_ACTIVITY,
    payload: data
  }
};

export const changeGuests = (data) => {
  return {
    type: SET_BOOKING_GUESTS,
    payload: data
  }
};
export const setBookingDatesTime = (data) => {
  // console.log(data)
  return {
    type: SET_BOOKING_DATE_TIME,
    payload: data
  }
};









