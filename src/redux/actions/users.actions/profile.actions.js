import {
  EDIT_REQUEST,
  EDIT_SUCCESS,
  EDIT_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
  CHECK_USER_EXISTENCE_REQUEST,
  CHECK_USER_EXISTENCE_SUCCESS,
  CHECK_USER_EXISTENCE_FAILURE,
  SEND_CONTACTS_SUCCESS,
  SEND_CONTACTS_FAILURE
} from '../../actionUserTypes';

export const getUserRequest = () => ({
  type: CHECK_USER_EXISTENCE_REQUEST
});

export const getUserSuccess = (data) => ({
  type: CHECK_USER_EXISTENCE_SUCCESS,
  payload: data
});

export const getUserFailure = (err) => ({
  type: CHECK_USER_EXISTENCE_FAILURE,
  payload: err
});

export const editRequest = () => ({
  type: EDIT_REQUEST
});

export const editSuccess = (data) => ({
  type: EDIT_SUCCESS,
  payload: data
});

export const editFailure = (err) => ({
  type: EDIT_FAILURE,
  payload: err
});

export const deleteUserRequest = () => ({
  type: DELETE_USER_REQUEST
});

export const deleteUserSuccess = (user) => ({
  type: DELETE_USER_SUCCESS,
  payload: user
});

export const deleteUserFailure = (err) => ({
  type: DELETE_USER_FAILURE,
  payload: err
});

export const sendContactsSuccess = () => ({
  type: SEND_CONTACTS_SUCCESS
});

export const sendContactsFailure = (err) => ({
  type: SEND_CONTACTS_FAILURE,
  payload: err
});
