import { ADMIN_REQUEST, ADMIN_SUCCESS, ADMIN_FAILURE } from '../../actionUserTypes';

export const adminRequest = () => ({
  type: ADMIN_REQUEST
});

export const adminSuccess = (data) => ({
  type: ADMIN_SUCCESS,
  payload: data
});

export const adminFailure = (err) => ({
  type: ADMIN_FAILURE,
  payload: err
});
