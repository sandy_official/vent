import * as actions from './profile.actions';
import history from '../../../history';
import axiosInstance from '../../../api';

export const getUser = () => async (dispatch) => {
  dispatch(actions.getUserRequest());
  try {
    const result = await axiosInstance.get('/api/user');
    dispatch(actions.getUserSuccess(result.data.user));
  } catch (error) {
    dispatch(actions.getUserFailure({ message: error.data }));
  }
};
export const editUser = (email, first_name, last_name, password) => async (dispatch) => {
  dispatch(actions.editRequest());
  try {
    const result = await axiosInstance.put(
      '/api/users',
      password ? { email, first_name, last_name, password } : { first_name, last_name, email }
    );
    dispatch(actions.editSuccess(result.data.user));
  } catch (error) {
    dispatch(actions.editFailure({ message: error.data }));
  }
};

export const deleteUser = () => async (dispatch) => {
  dispatch(actions.deleteUserRequest());
  try {
    const result = await axiosInstance.delete('/api/users');
    dispatch(actions.deleteUserSuccess(result.data));
    localStorage.removeItem('token');
    history.push('/home');
  } catch (error) {
    dispatch(actions.deleteUserFailure({ message: error.data }));
  }
};

export const sendContacts = (data) => async (dispatch) => {
  try {
    await axiosInstance.post('/api/contacts', { ...data });
    dispatch(actions.sendContactsSuccess());
  } catch (error) {
    dispatch(actions.sendContactsFailure({ message: error.data }));
  }
};
