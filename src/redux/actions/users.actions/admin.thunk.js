import * as actions from './admin.actions';
import axiosInstance from '../../../api';

export const getUsersList = () => async (dispatch) => {
  dispatch(actions.adminRequest());
  try {
    const result = await axiosInstance.get('/api/users');
    dispatch(actions.adminSuccess(result.data));
    // console.log(result.data);
  } catch (error) {
    dispatch(actions.adminFailure({ message: error.message }));
  }
};
