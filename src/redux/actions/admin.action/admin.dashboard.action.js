import {
  ADMIN_DASHBOARD_REQUEST,
  ADMIN_DASHBOARD_SUCCESS,
  ADMIN_DASHBOARD_FAILURE
} from '../../../redux/adminActionTypes';

export const adminDashboardRequest = () => ({
  type: ADMIN_DASHBOARD_REQUEST
});

export const adminDashboardSuccess = (data) => ({
  type: ADMIN_DASHBOARD_SUCCESS,
  payload: data
});

export const adminDashboardFailure = (err) => ({
  type: ADMIN_DASHBOARD_FAILURE,
  payload: err
});
