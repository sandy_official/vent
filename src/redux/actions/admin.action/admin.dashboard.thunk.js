import * as actions from './admin.dashboard.action';
import axiosInstance from '../../../api';

export const getAdminDashboard = () => async (dispatch) => {
  dispatch(actions.adminDashboardRequest());
  try {
    const result = await axiosInstance.get('/api/dashboard/users/');
    dispatch(actions.adminDashboardSuccess(result.data));
  } catch (error) {
    dispatch(actions.adminDashboardFailure({ message: error.message }));
  }
};
